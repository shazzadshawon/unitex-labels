<script type='text/javascript'>
    /*23.8950021,90.3121107      23.7824249,90.3934075*/
    /* <![CDATA[ */
    var factoryhub = {"factoryhub_back": "Back", "direction": ""};
    var factoryhubShortCode = {
        "map": {
            "fh_map_5ad2f3a73201a": {
                "style": "1",
                "type": "normal",
                "zoom": 12,
                "lat": [23.8950021, 23.7824249],
                "lng": [90.3121107, 90.3934075],
                "marker": "wp-content\/uploads\/sites\/5\/2017\/04\/maker.png",
                "height": 450,
                "info": ["Unitex Savar Factory", "Unitex Mohakhali Office"],
                "number": 2
            }
        }
    };
    /* ]]> */
</script>