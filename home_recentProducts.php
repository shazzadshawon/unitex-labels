<div class="vc_row wpb_row vc_row-fluid vc_custom_1490756979560" style="padding-top: 50px !important;">
    <div class="container">
        <div class="row">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner ">
                    <div class="wpb_wrapper">

                        <div class="vc_row wpb_row vc_inner vc_row-fluid vc_custom_1494390900006">
                            <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-4 vc_col-md-6">
                                <div class="vc_column-inner vc_custom_1494390908645">
                                    <div class="wpb_wrapper">
                                        <div class="fh-section-title clearfix text-left style-1">
                                            <h2>Our Recent Products</h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--<div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-8 vc_col-md-6">
                                <div class="vc_column-inner vc_custom_1494390915456">
                                    <div class="wpb_wrapper">
                                        <div class="wpb_text_column wpb_content_element ">
                                            <div class="wpb_wrapper">
                                                <p style="font-size: 20px; font-weight: 300;">Over 24
                                                    years experience &amp; knowledge of international
                                                    industrial systems, dedicated to provide the best
                                                    economical solutions to our valued customers.</p>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>-->
                            <div class="wpb_column vc_column_container vc_col-sm-1 vc_col-lg-1 vc_hidden-md vc_hidden-sm">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper"></div>
                                </div>
                            </div>
                        </div>

                        <div class="vc_row wpb_row vc_inner vc_row-fluid">
                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="fh-service  service-carousel">
                                            <div class="service-list "
                                                 id="service-slider-5ad2f2243ec20">

                                                <?php /*include('home_recentProducts_slide.php'); */?>

                                                <div class="item-service ">
                                                    <div class="service-content">
                                                        <div class="entry-thumbnail">
                                                            <div class="overlay" style="background-color:rgba(255,200,17,0.8)"></div>
                                                            <a href="product_detailsPage.php"></a>
                                                            <img width="370" height="230"
                                                                 src="recent_products/callerbone.jpg"
                                                                 class="attachment-factoryhub-blog-grid-3-thumb size-factoryhub-blog-grid-3-thumb wp-post-image"
                                                                 alt=""/>
                                                        </div>
                                                        <h2 class="entry-title"><a href="product_detailsPage.php">Woven Label</a></h2>
                                                        <p>We offer a broad variety of custom textile and w.oven labels to suit your ...</p>
                                                    </div>
                                                </div>

                                                <div class="item-service ">
                                                    <div class="service-content">
                                                        <div class="entry-thumbnail">
                                                            <div class="overlay" style="background-color:rgba(255,200,17,0.8)"></div>
                                                            <a href="product_detailsPage.php"></a>
                                                            <img width="370" height="230"
                                                                 src="recent_products/elastic_2.jpg"
                                                                 class="attachment-factoryhub-blog-grid-3-thumb size-factoryhub-blog-grid-3-thumb wp-post-image"
                                                                 alt=""/>
                                                        </div>
                                                        <h2 class="entry-title"><a href="product_detailsPage.php">Hang Tags</a></h2>
                                                        <p>With stylish and eye-catching custom hang tags, Price Tag & Photo Card  you can both promote your products ...</p>
                                                    </div>
                                                </div>

                                                <div class="item-service ">
                                                    <div class="service-content">
                                                        <div class="entry-thumbnail">
                                                            <div class="overlay" style="background-color:rgba(255,200,17,0.8)"></div>
                                                            <a href="product_detailsPage.php"></a>
                                                            <img width="370" height="230"
                                                                 src="recent_products/twilltape.jpg"
                                                                 class="attachment-factoryhub-blog-grid-3-thumb size-factoryhub-blog-grid-3-thumb wp-post-image"
                                                                 alt=""/>
                                                        </div>
                                                        <h2 class="entry-title"><a href="product_detailsPage.php">Printed Label</a></h2>
                                                        <p>Printed labels are primarily used to show material content, company logo, sizing and wash care in garments ...</p>
                                                    </div>
                                                </div>

                                                <div class="item-service ">
                                                    <div class="service-content">
                                                        <div class="entry-thumbnail">
                                                            <div class="overlay" style="background-color:rgba(255,200,17,0.8)"></div>
                                                            <a href="product_detailsPage.php"></a>
                                                            <img width="370" height="230"
                                                                 src="recent_products/hangtags.png"
                                                                 class="attachment-factoryhub-blog-grid-3-thumb size-factoryhub-blog-grid-3-thumb wp-post-image"
                                                                 alt=""/>
                                                        </div>
                                                        <h2 class="entry-title"><a href="product_detailsPage.php">Design Woven Tape</a></h2>
                                                        <p>Printed labels are primarily used to show material content, company logo, sizing and ...</p>
                                                    </div>
                                                </div>

                                                <div class="item-service ">
                                                    <div class="service-content">
                                                        <div class="entry-thumbnail">
                                                            <div class="overlay" style="background-color:rgba(255,200,17,0.8)"></div>
                                                            <a href="product_detailsPage.php"></a>
                                                            <img width="370" height="230"
                                                                 src="recent_products/callerbone.jpg"
                                                                 class="attachment-factoryhub-blog-grid-3-thumb size-factoryhub-blog-grid-3-thumb wp-post-image"
                                                                 alt=""/>
                                                        </div>
                                                        <h2 class="entry-title"><a href="product_detailsPage.php">Woven Label</a></h2>
                                                        <p>We offer a broad variety of custom textile and w.oven labels to suit your ...</p>
                                                    </div>
                                                </div>

                                                <div class="item-service ">
                                                    <div class="service-content">
                                                        <div class="entry-thumbnail">
                                                            <div class="overlay" style="background-color:rgba(255,200,17,0.8)"></div>
                                                            <a href="product_detailsPage.php"></a>
                                                            <img width="370" height="230"
                                                                 src="recent_products/elastic_2.jpg"
                                                                 class="attachment-factoryhub-blog-grid-3-thumb size-factoryhub-blog-grid-3-thumb wp-post-image"
                                                                 alt=""/>
                                                        </div>
                                                        <h2 class="entry-title"><a href="product_detailsPage.php">Hang Tags</a></h2>
                                                        <p>With stylish and eye-catching custom hang tags, Price Tag & Photo Card  you can both promote your products ...</p>
                                                    </div>
                                                </div>

                                                <div class="item-service ">
                                                    <div class="service-content">
                                                        <div class="entry-thumbnail">
                                                            <div class="overlay" style="background-color:rgba(255,200,17,0.8)"></div>
                                                            <a href="product_detailsPage.php"></a>
                                                            <img width="370" height="230"
                                                                 src="recent_products/twilltape.jpg"
                                                                 class="attachment-factoryhub-blog-grid-3-thumb size-factoryhub-blog-grid-3-thumb wp-post-image"
                                                                 alt=""/>
                                                        </div>
                                                        <h2 class="entry-title"><a href="product_detailsPage.php">Printed Label</a></h2>
                                                        <p>Printed labels are primarily used to show material content, company logo, sizing and wash care in garments ...</p>
                                                    </div>
                                                </div>

                                                <div class="item-service ">
                                                    <div class="service-content">
                                                        <div class="entry-thumbnail">
                                                            <div class="overlay" style="background-color:rgba(255,200,17,0.8)"></div>
                                                            <a href="product_detailsPage.php"></a>
                                                            <img width="370" height="230"
                                                                 src="recent_products/hangtags.png"
                                                                 class="attachment-factoryhub-blog-grid-3-thumb size-factoryhub-blog-grid-3-thumb wp-post-image"
                                                                 alt=""/>
                                                        </div>
                                                        <h2 class="entry-title"><a href="product_detailsPage.php">Design Woven Tape</a></h2>
                                                        <p>Printed labels are primarily used to show material content, company logo, sizing and ...</p>
                                                    </div>
                                                </div>

                                                <div class="item-service ">
                                                    <div class="service-content">
                                                        <div class="entry-thumbnail">
                                                            <div class="overlay" style="background-color:rgba(255,200,17,0.8)"></div>
                                                            <a href="product_detailsPage.php"></a>
                                                            <img width="370" height="230"
                                                                 src="recent_products/callerbone.jpg"
                                                                 class="attachment-factoryhub-blog-grid-3-thumb size-factoryhub-blog-grid-3-thumb wp-post-image"
                                                                 alt=""/>
                                                        </div>
                                                        <h2 class="entry-title"><a href="product_detailsPage.php">Woven Label</a></h2>
                                                        <p>We offer a broad variety of custom textile and w.oven labels to suit your ...</p>
                                                    </div>
                                                </div>

                                                <div class="item-service ">
                                                    <div class="service-content">
                                                        <div class="entry-thumbnail">
                                                            <div class="overlay" style="background-color:rgba(255,200,17,0.8)"></div>
                                                            <a href="product_detailsPage.php"></a>
                                                            <img width="370" height="230"
                                                                 src="recent_products/elastic_2.jpg"
                                                                 class="attachment-factoryhub-blog-grid-3-thumb size-factoryhub-blog-grid-3-thumb wp-post-image"
                                                                 alt=""/>
                                                        </div>
                                                        <h2 class="entry-title"><a href="product_detailsPage.php">Hang Tags</a></h2>
                                                        <p>With stylish and eye-catching custom hang tags, Price Tag & Photo Card  you can both promote your products ...</p>
                                                    </div>
                                                </div>

                                                <div class="item-service ">
                                                    <div class="service-content">
                                                        <div class="entry-thumbnail">
                                                            <div class="overlay" style="background-color:rgba(255,200,17,0.8)"></div>
                                                            <a href="product_detailsPage.php"></a>
                                                            <img width="370" height="230"
                                                                 src="recent_products/twilltape.jpg"
                                                                 class="attachment-factoryhub-blog-grid-3-thumb size-factoryhub-blog-grid-3-thumb wp-post-image"
                                                                 alt=""/>
                                                        </div>
                                                        <h2 class="entry-title"><a href="product_detailsPage.php">Printed Label</a></h2>
                                                        <p>Printed labels are primarily used to show material content, company logo, sizing and wash care in garments ...</p>
                                                    </div>
                                                </div>

                                                <div class="item-service ">
                                                    <div class="service-content">
                                                        <div class="entry-thumbnail">
                                                            <div class="overlay" style="background-color:rgba(255,200,17,0.8)"></div>
                                                            <a href="product_detailsPage.php"></a>
                                                            <img width="370" height="230"
                                                                 src="recent_products/hangtags.png"
                                                                 class="attachment-factoryhub-blog-grid-3-thumb size-factoryhub-blog-grid-3-thumb wp-post-image"
                                                                 alt=""/>
                                                        </div>
                                                        <h2 class="entry-title"><a href="product_detailsPage.php">Design Woven Tape</a></h2>
                                                        <p>Printed labels are primarily used to show material content, company logo, sizing and ...</p>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>