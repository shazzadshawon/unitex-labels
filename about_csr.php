<!DOCTYPE html>
<html lang="en-US">

<?php require('header_links.php'); ?>
<style id='factoryhub-inline-css' type='text/css'>
    .site-header .logo img {
        width: -999999999px;
        height: -999999999px;
    }

    .topbar {
        background-color: #f7f7f7;
    }

    .header-title {
        background-image: url(images/banner8_b.jpg);
    }

    .site-footer {
        background-color: #04192b;
    }

    .footer-widgets {
        background-color: #04192b;
    }

    .woocommerce form.checkout h3 input {
        top: 15px;
    }
</style>

<body class="blog  no-sidebar hfeed header-sticky hide-topbar-mobile blog-grid header-v1 footer- wpb-js-composer js-comp-ver-5.4.2 vc_responsive">
<div id="page" class="hfeed site">

    <?php require('header_top_white.php'); ?>
    <?php require('header_mid.php'); ?>

    <div class="page-header title-area style-1">
        <div class="header-title blog-title">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-title">Company CSR</h1></div>
                </div>
            </div>
        </div>
        <div class="breadcrumb-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <nav class="breadcrumb">
                            <span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
				                <a class="home" href="index.php" itemprop="url">
                                    <span itemprop="title">Home</span>
                                </a>
                            </span><i class="fa fa-angle-right" aria-hidden="true"></i>
                            <span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                                <span itemprop="title">Company CSR</span>
                            </span>
                        </nav>
                    </div>

                    <?php require('page_header_share.php'); ?>

                </div>
            </div>
        </div>
    </div>
    <div id="content" class="site-content">

        <div class="container">
            <div class="row">

                <div id="primary" class="content-area col-md-12">
                    <main id="main" class="site-main row">

                        <?php include('about_csr_slide.php'); ?>
                        <?php include('about_csr_slide.php'); ?>
                        <?php include('about_csr_slide.php'); ?>
                        <?php include('about_csr_slide.php'); ?>
                        <?php include('about_csr_slide.php'); ?>
                        <?php include('about_csr_slide.php'); ?>
                        <?php include('about_csr_slide.php'); ?>


                    </main><!-- #main -->
                </div><!-- #primary -->

            </div> <!-- .row -->
        </div> <!-- .container -->
    </div><!-- #content -->

    <?php require('footer.php'); ?>

</div><!-- #page -->

<?php require('common_modal.php'); ?>
<?php require('menu_mobile.php'); ?>

<?php require('scripts.php'); ?>

</body>

</html>
