<!DOCTYPE html>
<html lang="en-US">

<?php require('header_links.php') ?>
<style id='factoryhub-inline-css' type='text/css'>
    .site-header .logo img  {width:-999999999px; height:-999999999px; }.topbar { background-color: #f7f7f7; }.header-title { background-image: url(images/banner2_b.jpg); }.site-footer {background-color: #04192b;}.footer-widgets {background-color: #04192b;}.woocommerce form.checkout h3 input {  top: 15px;  }
</style>
<noscript><style>.woocommerce-product-gallery{ opacity: 1 !important; }</style></noscript>




<body class="product-template-default single single-product postid-1882 woocommerce woocommerce-page  single-right header-sticky hide-topbar-mobile blog-classic header-v1 footer- wpb-js-composer js-comp-ver-5.4.2 vc_responsive">
<div id="page" class="hfeed site">

    <?php require('header_top_white.php'); ?>

    <?php require('header_mid.php'); ?>


    <div class="page-header title-area style-1">
        <div class="header-title ">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-title">Product Name</h1></div>
                </div>
            </div>
        </div>
        <div class="breadcrumb-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <nav class="woocommerce-breadcrumb"><a href="index.php">Home</a><i
                                    class="fa fa-angle-right" aria-hidden="true"></i><a
                                    href="product_categoryPage.php">Product Category</a><i
                                    class="fa fa-angle-right" aria-hidden="true"></i>Product Name
                        </nav>
                    </div>
                    <?php require('page_header_share.php'); ?>
                </div>
            </div>
        </div>
    </div>
    <div id="content" class="site-content">

        <div class="container">
            <div class="row">

                <div id="primary" class="content-area col-md-8 col-sm-12 col-xs-12" style="margin-bottom: 100px;">
                    <div id="product-1882" class="post-1882 product type-product status-publish has-post-thumbnail product_cat-latest-technology product_cat-manufacturing product_tag-54 product_tag-company  first instock sale featured shipping-taxable purchasable product-type-variable has-default-attributes has-children">

                        <div class="product-details clearfix">
                            <div class="images-product">
                                <div class="woocommerce-product-gallery woocommerce-product-gallery--with-images woocommerce-product-gallery--columns-4 images" data-columns="4" style="opacity: 0; transition: opacity .25s ease-in-out;">
                                    <figure class="woocommerce-product-gallery__wrapper">
                                        <div data-thumb="http://demo2.steelthemes.com/factoryhub/wp-content/uploads/sites/5/2017/04/product2-180x180.png"
                                             class="woocommerce-product-gallery__image"><a
                                                    href="wp-content/uploads/sites/5/2017/04/product2.png"><img
                                                        width="270" height="270"
                                                        src="wp-content/uploads/sites/5/2017/04/product2.png"
                                                        class="attachment-shop_single size-shop_single wp-post-image"
                                                        alt="" title="product2" data-caption=""
                                                        data-src="http://demo2.steelthemes.com/factoryhub/wp-content/uploads/sites/5/2017/04/product2.png"
                                                        data-large_image="http://demo2.steelthemes.com/factoryhub/wp-content/uploads/sites/5/2017/04/product2.png"
                                                        data-large_image_width="270" data-large_image_height="270"
                                                        srcset="http://demo2.steelthemes.com/factoryhub/wp-content/uploads/sites/5/2017/04/product2.png 270w, http://demo2.steelthemes.com/factoryhub/wp-content/uploads/sites/5/2017/04/product2-150x150.png 150w, http://demo2.steelthemes.com/factoryhub/wp-content/uploads/sites/5/2017/04/product2-180x180.png 180w, http://demo2.steelthemes.com/factoryhub/wp-content/uploads/sites/5/2017/04/product2-75x75.png 75w, http://demo2.steelthemes.com/factoryhub/wp-content/uploads/sites/5/2017/04/product2-80x80.png 80w"
                                                        sizes="(max-width: 270px) 100vw, 270px"/></a></div>
                                    </figure>
                                </div>
                            </div>

                            <div class="summary entry-summary">

                                <h1 class="product_title entry-title">Product Name</h1>
                                <p class="price"><span class="woocommerce-Price-amount amount">Subtitle</span></p>
                                <div class="woocommerce-product-details__short-description">
                                    <p>Product Descrition Product Descrition  Product Descrition  Product Descrition  Product Descrition Product Descrition </p>
                                    <p>Product Descrition Product Descrition  Product Descrition  Product Descrition  Product Descrition Product Descrition </p>
                                    <p>Product Descrition Product Descrition  Product Descrition  Product Descrition  Product Descrition Product Descrition </p>
                                    <p>Product Descrition Product Descrition  Product Descrition  Product Descrition  Product Descrition Product Descrition </p>
                                </div>
                            </div><!-- .summary -->
                        </div>


                        <div class="woocommerce-tabs wc-tabs-wrapper">
                            <ul class="tabs wc-tabs" role="tablist">
                                <li class="description_tab" id="tab-title-description" role="tab"
                                    aria-controls="tab-description">
                                    <a href="#tab-description">Description</a>
                                </li>
                                <li class="additional_information_tab" id="tab-title-additional_information" role="tab"
                                    aria-controls="tab-additional_information">
                                    <a href="#tab-additional_information">Additional information</a>
                                </li>
                                <li class="reviews_tab" id="tab-title-reviews" role="tab" aria-controls="tab-reviews">
                                    <a href="#tab-reviews">Reviews (0)</a>
                                </li>
                            </ul>
                            <div class="woocommerce-Tabs-panel woocommerce-Tabs-panel--description panel entry-content wc-tab"
                                 id="tab-description" role="tabpanel" aria-labelledby="tab-title-description">
                                <h2>Description</h2>
                                <p>Product Descrition Product Descrition  Product Descrition  Product Descrition  Product Descrition Product Descrition </p>
                                <p>Product Descrition Product Descrition  Product Descrition  Product Descrition  Product Descrition Product Descrition </p>
                                <p>Product Descrition Product Descrition  Product Descrition  Product Descrition  Product Descrition Product Descrition </p>
                                <p>Product Descrition Product Descrition  Product Descrition  Product Descrition  Product Descrition Product Descrition </p>
                            </div>
                        </div>

                    </div><!-- #product-1882 -->


                </div>

                <aside id="primary-sidebar"
                       class="widgets-area primary-sidebar shop-sidebar col-xs-12 col-sm-12 col-md-4">
                    <div class="factoryhub-widget">
                        <div id="woocommerce_product_search-2" class="widget woocommerce widget_product_search">
                            <form role="search" method="get" class="woocommerce-product-search" action="product_detailsPage.php">
                                <label class="screen-reader-text" for="woocommerce-product-search-field">Searchfor:</label>
                                <input type="search" id="woocommerce-product-search-field" class="search-field" placeholder="Search..." value="" name="s" title="Search for:"/>
                                <input type="submit" value="Search"/>
                                <input type="hidden" name="post_type" value="product"/>
                            </form>
                        </div>

                        <?php include('productCategory_list.php'); ?>

                        <?php include('product_categoryPage_relatedProducts.php'); ?>
                    </div>
                </aside><!-- #secondary -->

                <?php require('contact_full_form.php'); ?>

            </div> <!-- .row -->
        </div> <!-- .container -->
    </div><!-- #content -->


    <?php require('footer.php'); ?>

</div><!-- #page -->

<?php require('common_modal.php'); ?>
<?php require('menu_mobile.php'); ?>

<script type="text/template" id="tmpl-variation-template">
    <div class="woocommerce-variation-description">
        {{{ data.variation.variation_description }}}
    </div>

    <div class="woocommerce-variation-price">
        {{{ data.variation.price_html }}}
    </div>

    <div class="woocommerce-variation-availability">
        {{{ data.variation.availability_html }}}
    </div>
</script>
<script type="text/template" id="tmpl-unavailable-variation-template">
    <p>Sorry, this product is unavailable. Please choose a different combination.</p>
</script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var wpcf7 = {"apiSettings":{"root":"http:\/\/demo2.steelthemes.com\/factoryhub\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"},"recaptcha":{"messages":{"empty":"Please verify that you are not a robot."}}};
    /* ]]> */
</script>

<?php require('scripts.php'); ?>
</body>

</html>
