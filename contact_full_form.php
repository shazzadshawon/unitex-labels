<div class="vc_row wpb_row vc_row-fluid vc_custom_1490585883088" style="margin-bottom: 50px;">
    <div class="container">
        <div class="row">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner ">
                    <div class="wpb_wrapper">
                        <div class="fh-section-title clearfix  text-center style-1" style="margin-bottom: 30px;">
                            <h2>Leave Your Message</h2>
                        </div>
                        <div class="wpb_text_column wpb_content_element  vc_custom_1490258102715" style="margin-top: 20px;">
                            <div class="wpb_wrapper">
                                <p style="font-weight: 300; color: #848484; text-align: center; width: 65%; margin: 0 auto;">
                                    If you have any questions about the services we provide simply use
                                    the form below. We try and respond to all queries and comments
                                    within 24 hours.</p>

                            </div>
                        </div>
                        <div class="vc_row wpb_row vc_inner vc_row-fluid vc_custom_1490258246649" style="margin-top: 50px;">
                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div role="form" class="wpcf7" id="wpcf7-f4-p203-o1"
                                             lang="en-US" dir="ltr">
                                            <div class="screen-reader-response"></div>
                                            <form action="http://demo2.steelthemes.com/factoryhub/contact/#wpcf7-f4-p203-o1"
                                                  method="post" class="wpcf7-form"
                                                  novalidate="novalidate">
                                                <div style="display: none;">
                                                    <input type="hidden" name="_wpcf7"/>
                                                    <input type="hidden" name="_wpcf7_version"/>
                                                    <input type="hidden" name="_wpcf7_locale"/>
                                                    <input type="hidden" name="_wpcf7_unit_tag"/>
                                                    <input type="hidden" name="_wpcf7_container_post"/>
                                                </div>
                                                <div class="fh-form fh-form-1">
                                                    <div class="row">
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <p class="field"><span
                                                                    class="wpcf7-form-control-wrap your-name"><input
                                                                        type="text" name="your-name"
                                                                        value="" size="40"
                                                                        class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                                                        aria-required="true"
                                                                        aria-invalid="false"
                                                                        placeholder="Your Name*"/></span>
                                                            </p>
                                                            <p class="field"><span
                                                                    class="wpcf7-form-control-wrap your-email"><input
                                                                        type="email"
                                                                        name="your-email" value=""
                                                                        size="40"
                                                                        class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
                                                                        aria-required="true"
                                                                        aria-invalid="false"
                                                                        placeholder="Email Address*"/></span>
                                                            </p>
                                                            <p class="field"><span
                                                                    class="wpcf7-form-control-wrap your-phone"><input
                                                                        type="text"
                                                                        name="your-phone" value=""
                                                                        size="40"
                                                                        class="wpcf7-form-control wpcf7-text"
                                                                        aria-invalid="false"
                                                                        placeholder="Phone"/></span>
                                                            </p>
                                                            <p class="field"><span
                                                                    class="wpcf7-form-control-wrap subject"><input
                                                                        type="text" name="subject"
                                                                        value="" size="40"
                                                                        class="wpcf7-form-control wpcf7-text"
                                                                        aria-invalid="false"
                                                                        placeholder="Subject"/></span>
                                                            </p>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <p class="field single-field"><span
                                                                    class="wpcf7-form-control-wrap your-message"><textarea
                                                                        name="your-message"
                                                                        cols="40" rows="10"
                                                                        class="wpcf7-form-control wpcf7-textarea"
                                                                        aria-invalid="false"
                                                                        placeholder="Your Message..."></textarea></span>
                                                            </p>
                                                        </div>
                                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                                            <p class="field submit"><input type="submit"
                                                                                           value="Submit Now"
                                                                                           class="wpcf7-form-control wpcf7-submit fh-btn"/>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="wpcf7-response-output wpcf7-display-none"></div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>