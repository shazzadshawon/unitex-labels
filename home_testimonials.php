<div class="vc_row wpb_row vc_row-fluid vc_custom_1493347633289 vc_row-has-fill parallax no-parallax-mobile">
    <div class="container">
        <div class="row">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner ">
                    <div class="wpb_wrapper">
                        <div class="vc_row wpb_row vc_inner vc_row-fluid vc_custom_1494391099608">
                            <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-3 vc_col-md-6">
                                <div class="vc_column-inner vc_custom_1494391105398">
                                    <div class="wpb_wrapper">
                                        <div class="fh-section-title clearfix  text-left style-2">
                                            <h2>Testimonals</h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-8 vc_col-md-6">
                                <div class="vc_column-inner vc_custom_1494391109734">
                                    <div class="wpb_wrapper">
                                        <div class="wpb_text_column wpb_content_element ">
                                            <div class="wpb_wrapper">
                                                <p style="font-size: 20px; font-weight: 300; color: #fff;">
                                                    We go to great lengths to identify impressive new
                                                    talent self starters with specific skill sets to
                                                    produce truly amazing results.</p>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="wpb_column vc_column_container vc_col-sm-1 vc_col-lg-1 vc_hidden-md vc_hidden-sm">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper"></div>
                                </div>
                            </div>
                        </div>
                        <div class="vc_row wpb_row vc_inner vc_row-fluid">
                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="fh-testimonials  style-1 carousel">
                                            <div class="testi-list " id="testimonial-slider-5ad2f2251b907">

                                                <div class="testi-item ">
                                                    <div class="testi-content">
                                                        <div class="info clearfix">
                                                            <img width="80" height="80"
                                                                 src="images/testimonials/testi-1-80x80.jpg"
                                                                 class="attachment-factoryhub-testimonial-thumb size-factoryhub-testimonial-thumb wp-post-image"
                                                                 alt=""
                                                                 srcset="images/testimonials/testi-1-80x80.jpg 80w, images/testimonials/testi-1-80x80.jpg 75w"
                                                                 sizes="(max-width: 80px) 100vw, 80px"/>
                                                            <h4 class="testi-name">Client Company</h4>
                                                            <span class="testi-job">Client Name</span>
                                                        </div>
                                                        <div class="testi-des">They have got my project
                                                            on time with the competition with a highly
                                                            seds our skilled, well-organized and
                                                            experienced team of professional Engineers.
                                                        </div>
                                                        <i class="fa fa-quote-right"
                                                           aria-hidden="true"></i>
                                                    </div>
                                                </div>

                                                <?php include('home_testimonials_slide.php'); ?>
                                                <?php include('home_testimonials_slide.php'); ?>
                                                <?php include('home_testimonials_slide.php'); ?>
                                                <?php include('home_testimonials_slide.php'); ?>
                                                <?php include('home_testimonials_slide.php'); ?>
                                                <?php include('home_testimonials_slide.php'); ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>