<!DOCTYPE html>
<html lang="en-US">

<?php require('header_links.php') ?>
<style id='factoryhub-inline-css' type='text/css'>
    .site-header .logo img {
        width: -999999999px;
        height: -999999999px;
    }

    .topbar {
        background-color: #f7f7f7;
    }

    .header-title {
        background-image: url(images/banner_b.jpg);
    }

    .site-footer {
        background-color: #04192b;
    }

    .footer-widgets {
        background-color: #04192b;
    }

    .woocommerce form.checkout h3 input {
        top: 15px;
    }
</style>

<body class="archive post-type-archive post-type-archive-product woocommerce woocommerce-page  single-right hfeed header-sticky hide-topbar-mobile blog-classic header-v1 footer- wpb-js-composer js-comp-ver-5.4.2 vc_responsive">
<div id="page" class="hfeed site">

    <?php require('header_top_white.php'); ?>

    <?php require('header_mid.php'); ?>


    <div class="page-header title-area style-1">
        <div class="header-title ">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-title">Products</h1></div>
                </div>
            </div>
        </div>
        <div class="breadcrumb-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <nav class="woocommerce-breadcrumb"><a href="index.php">Home</a><i class="fa fa-angle-right"
                                                                                           aria-hidden="true"></i>Shop
                        </nav>
                    </div>
                    <?php require('page_header_share.php'); ?>
                </div>
            </div>
        </div>
    </div>

    <div id="content" class="site-content">

        <div class="container">
            <div class="row">
                <div id="primary" class="content-area col-md-9 col-sm-12 col-xs-12" style="margin-bottom: 100px;">

                    <div class="shop-toolbar">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <p class="woocommerce-result-count">Showing 1&ndash;10 of 10 results</p>
                            </div>
                        </div>
                    </div>

                    <ul class="products">

                        <li class="post-1882 product type-product status-publish has-post-thumbnail product_cat-latest-technology product_cat-manufacturing product_tag-54 product_tag-company  first instock sale featured shipping-taxable purchasable product-type-variable has-default-attributes has-children col-sm-6 col-xs-6 col-md-4">
                            <div class="product-inner">
                                <a href="product_detailsPage.php"
                                   class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
                                    <div class="product-header"><a rel="nofollow" href="product_detailsPage.php"
                                                                   data-quantity="1" data-product_id="1882"
                                                                   data-product_sku=""
                                                                   class="button product_type_variable add_to_cart_button">View
                                            Details</a><img width="270" height="270" src="images/products/product1.png"
                                                            class="attachment-shop_catalog size-shop_catalog wp-post-image"
                                                            alt=""
                                                            srcset="images/products/product1.png 270w, images/products/product1.png 150w"
                                                            sizes="(max-width: 270px) 100vw, 270px"/></div>
                                    <h2 class="woocommerce-loop-product__title">Product Name</h2>
                                    <span class="price"><span class="woocommerce-Price-amount amount">
                                    <span class="woocommerce-Price-currencySymbol"></span>Short Descripee</span>
                                </a>
                            </div>
                        </li>

                        <li class="post-1885 product type-product status-publish has-post-thumbnail product_cat-agriculture product_tag-books product_tag-classic product_tag-engines  instock featured shipping-taxable purchasable product-type-simple col-sm-6 col-xs-6 col-md-4">
                            <div class="product-inner">
                                <a href="product_detailsPage.php"
                                   class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
                                    <div class="product-header"><a rel="nofollow" href="product_detailsPage.php"
                                                                   data-quantity="1" data-product_id="1885"
                                                                   data-product_sku=""
                                                                   class="button product_type_simple add_to_cart_button">View
                                            Details</a><img width="270" height="270" src="images/products/product2.png"
                                                            class="attachment-shop_catalog size-shop_catalog wp-post-image"
                                                            alt=""
                                                            srcset="images/products/product2.png 270w, images/products/product2.png 150w"
                                                            sizes="(max-width: 270px) 100vw, 270px"/></div>
                                    <h2 class="woocommerce-loop-product__title">Product Name</h2>
                                    <span class="price"><span class="woocommerce-Price-amount amount">
                                    <span class="woocommerce-Price-currencySymbol"></span>Short Descripee</span>
                                </a>
                            </div>
                        </li>

                        <li class="post-1885 product type-product status-publish has-post-thumbnail product_cat-agriculture product_tag-books product_tag-classic product_tag-engines  instock featured shipping-taxable purchasable product-type-simple col-sm-6 col-xs-6 col-md-4">
                            <div class="product-inner">
                                <a href="product_detailsPage.php"
                                   class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
                                    <div class="product-header"><a rel="nofollow" href="product_detailsPage.php"
                                                                   data-quantity="1" data-product_id="1885"
                                                                   data-product_sku=""
                                                                   class="button product_type_simple add_to_cart_button">View
                                            Details</a><img width="270" height="270" src="images/products/product3.png"
                                                            class="attachment-shop_catalog size-shop_catalog wp-post-image"
                                                            alt=""
                                                            srcset="images/products/product3.png 270w, images/products/product3.png 150w"
                                                            sizes="(max-width: 270px) 100vw, 270px"/></div>
                                    <h2 class="woocommerce-loop-product__title">Product Name</h2>
                                    <span class="price"><span class="woocommerce-Price-amount amount">
                                    <span class="woocommerce-Price-currencySymbol"></span>Short Descripee</span>
                                </a>
                            </div>
                        </li>

                        <?php require('product_categoryPage_slide.php'); ?>
                        <?php require('product_categoryPage_slide.php'); ?>
                        <?php require('product_categoryPage_slide.php'); ?>
                        <?php require('product_categoryPage_slide.php'); ?>
                        <?php require('product_categoryPage_slide.php'); ?>
                        <?php require('product_categoryPage_slide.php'); ?>
                        <?php require('product_categoryPage_slide.php'); ?>
                        <?php require('product_categoryPage_slide.php'); ?>

                    </ul>
                </div>

                <aside id="primary-sidebar"
                       class="widgets-area primary-sidebar shop-sidebar col-xs-12 col-sm-12 col-md-3">
                    <div class="factoryhub-widget">
                        <div id="woocommerce_product_search-2" class="widget woocommerce widget_product_search">
                            <form role="search" method="get" class="woocommerce-product-search"
                                  action="product_categoryPage.php">
                                <label class="screen-reader-text"
                                       for="woocommerce-product-search-field">Searchfor:</label>
                                <input type="search" id="woocommerce-product-search-field" class="search-field"
                                       placeholder="Search..." value="" name="s" title="Search for:"/>
                                <input type="submit" value="Search"/>
                                <input type="hidden" name="post_type" value="product"/>
                            </form>
                        </div>
                        <?php include('productCategory_list.php'); ?>

                        <?php include('product_categoryPage_relatedProducts.php'); ?>

                    </div>
                </aside><!-- #secondary -->

                <?php require('about_numbers.php'); ?>
                <?php require('contact_full_form.php'); ?>

            </div> <!-- .row -->
        </div> <!-- .container -->
    </div><!-- #content -->


    <?php require('footer.php'); ?>

</div><!-- #page -->

<?php require('common_modal.php'); ?>
<?php require('menu_mobile.php'); ?>

<?php require('scripts.php'); ?>

</body>

</html>
