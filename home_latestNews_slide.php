<div class="item-latest-post ">
    <div class="entry-thumbnail"><a
            href="blog_detail.php"><img
                width="370" height="230"
                src="images/home_blog/370_230.jpg"
                class="attachment-factoryhub-blog-grid-3-thumb size-factoryhub-blog-grid-3-thumb wp-post-image"
                alt=""/><i class="fa fa-link" aria-hidden="true"></i></a>
    </div>
    <div class="entry-header">
        <div class="entry-meta"><span
                class="meta posted-on"><a
                    href="blog_detail.php"
                    rel="bookmark"><i
                        class="fa fa-clock-o"
                        aria-hidden="true"></i><time
                        class="entry-date published updated"
                        datetime="2017-04-20T08:57:22+00:00">April 24, 2018</time></a></span>
        </div>
        <h2 class="entry-title"><a
                href="blog_detail.php">Blog Title Blog Title Blog Title</a></h2>
    </div>
    <div class="line"></div>
</div>