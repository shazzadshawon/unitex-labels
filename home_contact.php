<div class="vc_row wpb_row vc_row-fluid vc_custom_1493095504602 vc_row-has-fill">
    <div class="container">
        <div class="row">
            <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-4 vc_col-md-6">
                <div class="vc_column-inner ">
                    <div class="wpb_wrapper">
                        <div class="fh-section-title clearfix  text-left style-1">
                            <h2>Get In Touch With Us</h2>
                        </div>
                        <div class="wpb_text_column wpb_content_element  vc_custom_1492585572115">
                            <div class="wpb_wrapper">
                                <div><span class="main-color"
                                           style="font-size: 20px; padding-right: 10px;">For Business:</span>
                                    Do you have questions about how Factory HUB can help your company?
                                    Send us an email and we’ll get in touch shortly, or phone 1800 234
                                    567 between 07:30 and 19:00 Monday to Friday — we would be delighted
                                    to speak.
                                </div>

                            </div>
                        </div>

                        <div class="wpb_text_column wpb_content_element ">
                            <div class="wpb_wrapper">
                                <div><span class="main-color"
                                           style="font-size: 20px; padding-right: 10px;">Note:</span>Your
                                    details are kept strictly confidential as per our Company Privacy
                                    Policy.
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php include('contact_form.php'); ?>

        </div>
    </div>
</div>