<div class="site-menu">
    <div class="container">
        <div class="row">
            <div class="site-nav col-sm-12 col-xs-12 col-md-8">
                <div class="navbar-toggle toggle-navs">
                    <a href="#" class="navbars-icon">
                        <i class="fa fa-bars" aria-hidden="true"></i>
                    </a>
                </div>
                <nav id="site-navigation" class="main-nav primary-nav nav">
                    <ul id="primary-menu" class="menu">
                        <li id="menu-item-219" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-219">
                            <a href="index.php">Home</a></li>


                        <li id="menu-item-139"
                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-139">
                            <a href="about_companyProfile.php">About</a>
                            <ul class="sub-menu">

                                <li id="menu-item-2672"
                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-2672">
                                    <a href="about_companyProfile.php">Profile</a>
                                    <ul class="sub-menu">
                                        <li id="menu-item-2680"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2680">
                                            <a href="about_companyProfile.php#companyProfile">Company Profile</a></li>
                                        <li id="menu-item-2679"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2679">
                                            <a href="about_companyProfile.php#boardandmanagement">Board and Management</a>
                                        </li>
                                    </ul>
                                </li>

                                <li id="menu-item-2672"
                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-2672">
                                    <a href="about_ceoMessage.php">Messages</a>
                                    <ul class="sub-menu">
                                        <li id="menu-item-2680"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2680">
                                            <a href="about_ceoMessage.php#ceoMessage">CEO Message</a></li>
                                        <li id="menu-item-2679"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2679">
                                            <a href="about_ceoMessage.php#companyMission">Company Mission</a>
                                        </li>
                                        <li id="menu-item-2679"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2679">
                                            <a href="about_ceoMessage.php#companyVision">Company Vision</a>
                                        </li>
                                    </ul>
                                </li>

                                <li id="menu-item-2672"
                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-2672">
                                    <a href="about_certification.php">Achievements</a>
                                    <ul class="sub-menu">
                                        <li id="menu-item-2680"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2680">
                                            <a href="about_certification.php#awardCertificate">Awards &amp; Certifications</a></li>
                                        <li id="menu-item-2679"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2679">
                                            <a href="about_certification.php#companyMembership">Membership</a>
                                        </li>
                                    </ul>
                                </li>

                                <li id="menu-item-217"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-217">
                                    <a href="about_csr.php">Company CSR</a></li>

                            </ul>
                        </li>

                        <li id="menu-item-216"
                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-216">
                            <a href="product_categoryPage.php">Products</a>
                            <ul class="sub-menu">
                                <li id="menu-item-217"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-217">
                                    <a href="product_categoryPage.php">Woven Label</a></li>
                                <li id="menu-item-218"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-218">
                                    <a href="product_categoryPage.php">Printed Label</a></li>
                                <li id="menu-item-218"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-218">
                                    <a href="product_categoryPage.php">Hang Tags, Price tags, Photo
                                        Cards</a></li>
                                <li id="menu-item-218"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-218">
                                    <a href="product_categoryPage.php">Caller Insert, Caller Bone</a></li>
                                <li id="menu-item-218"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-218">
                                    <a href="product_categoryPage.php">Poly</a></li>
                                <li id="menu-item-218"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-218">
                                    <a href="product_categoryPage.php">Metal Button</a></li>
                            </ul>
                        </li>
                        <li id="menu-item-140"
                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-140">
                            <a href="machineries_categoryPage.php">Machineries</a>
                            <ul class="sub-menu">
                                <li id="menu-item-146"
                                    class="menu-item menu-item-type-post_type menu-item-object-service menu-item-146">
                                    <a href="machineries_categoryPage.php">Cutting Machine</a></li>
                                <li id="menu-item-145"
                                    class="menu-item menu-item-type-post_type menu-item-object-service menu-item-145">
                                    <a href="machineries_categoryPage.php">Offset Mechine</a></li>
                                <li id="menu-item-144"
                                    class="menu-item menu-item-type-post_type menu-item-object-service menu-item-144">
                                    <a href="machineries_categoryPage.php">Woven Label Mechine</a></li>
                                <li id="menu-item-143"
                                    class="menu-item menu-item-type-post_type menu-item-object-service menu-item-143">
                                    <a href="machineries_categoryPage.php">Printed Label Mechine</a></li>

                            </ul>
                        </li>
                        <li id="menu-item-139"
                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-139">
                            <a href="#">Others</a>
                            <ul class="sub-menu">
                                <li id="menu-item-2672"
                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-2672">
                                    <a href="whyWithus.php">Why with Us</a>
                                    <ul class="sub-menu">
                                        <li id="menu-item-2680"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2680">
                                            <a href="whyWithus.php#1">Quality Assurance</a></li>
                                        <li id="menu-item-2679"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2679">
                                            <a href="whyWithus.php#2">Sustainability</a>
                                        </li>
                                        <li id="menu-item-2678"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2678">
                                            <a href="whyWithus.php#3">Green technology</a></li>
                                        <li id="menu-item-2678"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2678">
                                            <a href="whyWithus.php#4">Health &amp; Safety</a></li>
                                        <li id="menu-item-2678"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2678">
                                            <a href="whyWithus.php#5">Code of Conduct</a></li>
                                        <li id="menu-item-2678"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2678">
                                            <a href="whyWithus.php#6">Workers benifit</a></li>
                                        <li id="menu-item-2678"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2678">
                                            <a href="whyWithus.php#7">Compliance</a>
                                        </li>
                                        <li id="menu-item-2678"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2678">
                                            <a href="whyWithus.php#8">Transportation</a>
                                        </li>

                                    </ul>
                                </li>
                                <li id="menu-item-2671"
                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-2671">
                                    <a href="factory.php">Factory &amp; Units</a>
                                    <ul class="sub-menu">
                                        <li id="menu-item-2675"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2675">
                                            <a href="factory.php#1">Capacity &amp; Machinaries</a></li>
                                        <li id="menu-item-2676"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2676">
                                            <a href="factory.php#2">Location</a>
                                        </li>
                                        <li id="menu-item-2677"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2677">
                                            <a href="factory.php#3">Photos</a>
                                        </li>
                                        <li id="menu-item-2677"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2677">
                                            <a href="factory.php#4">Environment Management</a>
                                        </li>
                                    </ul>
                                </li>

                                <li id="menu-item-2671"
                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-2671">
                                    <a href="ourClient.php">Our Clients</a>
                                    <ul class="sub-menu">
                                        <li id="menu-item-2675"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2675">
                                            <a href="ourClient.php#1">Client Lists</a></li>
                                        <li id="menu-item-2676"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2676">
                                            <a href="ourClient.php#2">Testimonials</a>
                                        </li>

                                    </ul>
                                </li>

                                <li id="menu-item-215"
                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-215">
                                    <a href="blog.php">News &amp; Events</a>
                                    <ul class="sub-menu">
                                        <li id="menu-item-220"
                                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-220">
                                            <a href="blog.php">Recent Achievements</a></li>
                                        <li id="menu-item-223"
                                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-223">
                                            <a href="blog.php">Certifications</a></li>
                                        <li id="menu-item-222"
                                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-222">
                                            <a href="blog.php">New Locations</a></li>
                                        <li id="menu-item-221"
                                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-221">
                                            <a href="blog.php">New Announcements</a></li>
                                        <li id="menu-item-2642"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2642">
                                            <a href="blog.php">New partners</a></li>
                                        <li id="menu-item-2642"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2642">
                                            <a href="blog.php">New clients</a></li>
                                        <li id="menu-item-2642"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2642">
                                            <a href="blog.php">New Units Opening</a></li>
                                    </ul>
                                </li>

                            </ul>
                        </li>

                        <li id="menu-item-2671"
                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2671">
                            <a href="gallery.php">Gallery</a>
                        </li>

                        <li id="menu-item-219"
                            class="menu-item menu-item-type-post_type menu-item-object-custom menu-item-219">
                            <a href="contact.php">Contact</a>
                            <!--<ul class="sub-menu">
                                <li id="menu-item-220"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-220">
                                    <a href="contact.php#1">Contact Form</a></li>
                                <li id="menu-item-223"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-223">
                                    <a href="contact.php#2">Google Map Location</a></li>
                                <li id="menu-item-221"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-221">
                                    <a href="contact.php#3">Contact Details</a></li>
                            </ul>-->
                        </li>

                    </ul>
                </nav>
            </div>
            <div class="col-md-4 col-sm-12 col-xs-12 extra-item menu-item-text">
                <div class="right" style="float: right;">
                    <i class="flaticon-technology-1"></i>
                    <div class="extra-text">
                        <p class="number" style="line-height: unset !important;">(+880) 1713 175906</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>