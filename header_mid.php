<header id="masthead" class="site-header clearfix">

    <div class="header-main clearfix">
        <div class="site-contact">
            <div class="container">
                <div class="row">
                    <div class="site-logo col-md-3 col-sm-6 col-xs-6">
                        <a href="index.php" class="logo">
                            <img src="images/logo_4.png" alt="UNITEX" class="logo-light hide-logo" style="width: 50%;">
                            <img src="images/logo_4.png" alt="UNITEX" class="logo-dark show-logo" style="width: 50%;">
                        </a>

                    </div>
                    <div class="site-extra-text col-md-9 col-sm-6 col-xs-6">
                        <div class="extra-item item-text">
                            <div class="style-1">
                                <div class="position item">
                                    <i class="factory-check"></i>
                                    <div><span>Number #1</span>Industrial Theme</div>
                                </div>

                                <div class="certified item">
                                    <i class="factory-check"></i>
                                    <div><span>Certified</span>ISO 9001:2008</div>
                                </div>

                                <div class="the-best item">
                                    <i class="factory-check"></i>
                                    <div><span>The Best</span>Solution Provider</div>
                                </div>

                                <div class="social">
                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                    <a href="#"><i class="fa fa-skype"></i></a>
                                    <a href="#"><i class="fa fa-linkedin"></i></a>
                                </div>
                            </div>

                            <div class="style-2">
                                <div class="location item-2">
                                    <i class="flaticon-pin"></i>
                                    <div>
                                        <span>Visit our Location :</span>
                                        Mohakhali DOHS, Dhaka-1206
                                    </div>
                                </div>

                                ​
                                <div class="opening item-2">
                                    <i class="flaticon-clock"></i>
                                    <div>
                                        <span>Opening Hours :</span>
                                        Mon - Fri: 8am - 5pm
                                    </div>
                                </div>

                                ​
                                <div class="contact item-2">
                                    <i class="flaticon-web"></i>
                                    <div>
                                        <span>Send us a Mail:</span>
                                        Info@unitex.com
                                    </div>
                                </div>

                                <div class="item-button">
                                    <a href="#" class="btn fh-btn">Get a Quote</a>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <?php require('menu.php') ?>

    </div>

</header><!-- #masthead -->