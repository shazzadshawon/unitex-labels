<!DOCTYPE html>
<html lang="en-US">

<?php require('header_links.php') ?>

<body class="home page-template page-template-template-homepage page-template-template-homepage-php page page-id-11  no-sidebar header-sticky hide-topbar-mobile blog-classic header-v1 footer- wpb-js-composer js-comp-ver-5.4.2 vc_responsive">
<div id="page" class="hfeed site">

    <?php require('header_top.php'); ?>
    <?php require('header_mid.php'); ?>



    <div id="content" class="site-content">

        <div class="container-fluid">
            <div class="row">

                <?php require('home_slider.php') ?>

                <?php require('home_recentProducts.php') ?>

                <?php require('home_banner.php') ?>

                <?php require('home_aboutSection.php'); ?>

                <?php require('home_photoGallery.php'); ?>

                <div class="vc_row-full-width vc_clearfix"></div>

<!--                --><?php //require('home_banner_title.php'); ?>

                <?php require('home_latestNews.php'); ?>

                <?php require('home_testimonials.php'); ?>

                <?php require('home_contact.php'); ?>

            </div> <!-- .row -->
        </div> <!-- .container -->
    </div><!-- #content -->

    <?php require('footer.php'); ?>

</div><!-- #page -->

<?php require('common_modal.php'); ?>
<?php require('menu_mobile.php'); ?>

<?php require('scripts.php'); ?>

</body>
</html>
