<div class="testi-item ">
    <div class="testi-content">
        <div class="info clearfix">
            <img width="80" height="80"
                 src="images/testimonials/80x80_neuter.jpg"
                 class="attachment-factoryhub-testimonial-thumb size-factoryhub-testimonial-thumb wp-post-image"
                 alt=""
                 srcset="images/testimonials/80x80_neuter.jpg 80w, images/testimonials/80x80_neuter.jpg 75w"
                 sizes="(max-width: 80px) 100vw, 80px"/>
            <h4 class="testi-name">Client Company</h4>
            <span class="testi-job">Client Name</span>
        </div>
        <div class="testi-des">They have got my project
            on time with the competition with a highly
            seds our skilled, well-organized and
            experienced team of professional Engineers.
        </div>
        <i class="fa fa-quote-right"
           aria-hidden="true"></i>
    </div>
</div>