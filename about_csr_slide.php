<article id="post-57" class="blog-wrapper col-2 col-xs-12 col-md-6 col-sm-6 post-57 post type-post status-publish format-standard has-post-thumbnail hentry category-entrepreneurs category-latest-technology ">
    <div class="entry-thumbnail">
        <a href="#"><img
                    width="570" height="300"
                    src="images/570_300.png"
                    class="attachment-factoryhub-blog-grid-2-thumb size-factoryhub-blog-grid-2-thumb wp-post-image"
                    alt=""
                    srcset="images/570_300.png 570w, images/570_300.png 300w"
                    sizes="(max-width: 570px) 100vw, 570px"/>
        <i class="fa fa-link"aria-hidden="true"></i></a>
    </div>
    <header class="entry-header">
        <div class="entry-meta">
            <span class="meta posted-on">
                <a href="#" rel="bookmark">
                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                    <time class="entry-date published updated">April 20, 2018</time>
                </a>
            </span>
            <span class="meta cat-link">
				<a href="#" class="category-link"><i class="fa fa-tags" aria-hidden="true"></i>TAG</a>
            </span>
        </div><!-- .entry-meta -->

        <h2 class="entry-title">
            <a href="#">Company CSR Subject Title / Focus Area</a>
        </h2>
    </header><!-- .entry-header -->

    <div class="entry-content">
        <p>Company CSR Short Details or Description. Company CSR Short Details or Description. Company CSR Short Details or Description.</p>
    </div><!-- .entry-content -->

    <footer class="entry-footer clearfix"></footer>
</article><!-- #post-## -->