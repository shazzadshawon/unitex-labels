<!DOCTYPE html>
<html lang="en-US">

<?php require('header_links.php'); ?>
<link href="css/lsb.css" rel="stylesheet" type="text/css">
<link href="css/jquery.fancybox.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/snow.css" rel="stylesheet" type="text/css" media="all" /><!-- stylesheet -->
<link href="css/myStyle.css" rel="stylesheet" type="text/css" media="all">


<style id='factoryhub-inline-css' type='text/css'>

    .site-header .logo img {
        width: -999999999px;
        height: -999999999px;
    }

    .topbar {
        background-color: #f7f7f7;
    }

    .header-title {
        background-image: url(images/banner11_b.png);
    }

    .site-footer {
        background-color: #04192b;
    }

    .footer-widgets {
        background-color: #04192b;
    }

    .woocommerce form.checkout h3 input {
        top: 15px;
    }
</style>

<body class="page-template page-template-template-fullwidth page-template-template-fullwidth-php page page-id-196  no-sidebar header-sticky hide-topbar-mobile blog-classic header-v1 footer- wpb-js-composer js-comp-ver-5.4.2 vc_responsive">
<div id="page" class="hfeed site">

    <?php require('header_top_white.php'); ?>
    <?php require('header_mid.php'); ?>


    <div class="page-header title-area style-2">
        <div class="header-title">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-title">Gallery</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="breadcrumb-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <nav class="breadcrumb">
                            <span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
				                <a class="home" href="index.php" itemprop="url"><span itemprop="title">Home</span></a>
			                </span>
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                            <span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                                <span itemprop="title">Gallery</span>
                            </span>
                        </nav>
                    </div>

                    <?php require('page_header_share.php'); ?>

                </div>
            </div>
        </div>
    </div>
    <div id="content" class="site-content">

        <div id="companyProfile" class="container-fluid">
            <div class="row">


                <div id="boardandmanagement" class="vc_row wpb_row vc_row-fluid vc_custom_1490597375444 gallery" style="margin-top: 70px; margin-bottom: 100px;">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="fh-section-title clearfix  text-center style-1">
                                            <h2>Gallery</h2>
                                        </div>
                                        <div class="vc_row wpb_row vc_inner vc_row-fluid vc_custom_1490588863772">

                                            <img class="img-responsive loader_image" src="images/loading.gif" style="margin: 0 auto">
                                            <section class="gallery-part" id="gallary">
                                                <div class="gallery_loader" style="display: none">
                                                    <div class="tabbing-wrapper">
                                                        <?php include('gallery_items.php'); ?>
                                                    </div>
                                                </div>
                                                <div class="gallery-blog">
                                                    <ul class="gallery-img-sec clearfix"></ul>
                                                </div>
                                            </section>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <?php require('about_numbers.php'); ?>
                <?php require('contact_locationMap.php'); ?>
            </div> <!-- .row -->
        </div> <!-- .container -->
    </div><!-- #content -->


    <?php require('footer.php'); ?>

</div><!-- #page -->

<?php require('common_modal.php'); ?>
<?php require('menu_mobile.php'); ?>

<script type='text/javascript'>
    /* <![CDATA[ */
    var wpcf7 = {
        "apiSettings": {
            "root": "http:\/\/demo2.steelthemes.com\/factoryhub\/wp-json\/contact-form-7\/v1",
            "namespace": "contact-form-7\/v1"
        }, "recaptcha": {"messages": {"empty": "Please verify that you are not a robot."}}
    };
    /* ]]> */
</script>

<link rel='stylesheet' id='font-awesome-css'
      href='wp-content/plugins/js_composer/assets/lib/bower/font-awesome/css/font-awesome.min7661.css?ver=5.4.2'
      type='text/css' media='all'/>


<?php require('scripts.php'); ?>
<?php require('contanct_map_scripts.php'); ?>

<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script><!-- Required-js -->
<script src="js/isotope.pkgd.min.js"></script> <!-- Filering -->
<script src="js/packery-mode.pkgd.min.js"></script>
<script src="js/jquery.fancybox.js"></script>
<script src="js/responsiveslides.min.js"></script>


<script>
    // Filltering
    var $container;
    $(document).ready(function(){

        var event_folder = "images/gallery/products/";
        $.ajax({
            url : event_folder,
            success: function (event_data) {
                $(event_data).find("a").attr("href", function (i, val) {
                    if( val.match(/\.(jpe?g|png)$/) ) {
//                            $("body").append( "<img src='"+ folder + val +"'>" );
                        var src = val;
                        var products = '<li class="main-item grid-sizer all products">'+
                            '<div class="grid">' +
                            '<a href="'+src+'" class="lsb-preview" data-lsb-group="header">' +
                            '<figure class="effect-winston">' +
                            '<img src="'+src+'" class="img-responsive" alt=" " />' +
                            '</figure>' +
                            '</a>' +
                            '</div>' +
                            '</li>';
                        $('.gallery-img-sec').append(products);
                    }
                });
                loadMachineries();
            }
        });
    });

    function loadMachineries(){
        var folder = "images/gallery/machineries/";
        $.ajax({
            url : folder,
            success: function (music_data) {
                $(music_data).find("a").attr("href", function (i, val) {
                    if( val.match(/\.(jpe?g|png)$/) ) {
                        var src = val;
                        var machineries = '<li class="main-item grid-sizer all machineries">'+
                            '<div class="grid">' +
                            '<a href="'+src+'" class="lsb-preview" data-lsb-group="header">' +
                            '<figure class="effect-winston">' +
                            '<img src="'+src+'" class="img-responsive" alt=" " />' +
                            '</figure>' +
                            '</a>' +
                            '</div>' +
                            '</li>';
                        $('.gallery-img-sec').append(machineries);
                    }
                });
                loadFactory();
            }
        });
    }

    function loadFactory(){
        var wedding_folder = "images/gallery/factory/";
        $.ajax({
            url : wedding_folder,
            success: function (wedding_data) {
                $(wedding_data).find("a").attr("href", function (i, val) {
                    if( val.match(/\.(jpe?g|png)$/) ) {
                        var src = val;
                        var factory = '<li class="main-item grid-sizer all factory">'+
                            '<div class="grid">' +
                            '<a href="'+src+'" class="lsb-preview" data-lsb-group="header">' +
                            '<figure class="effect-winston">' +
                            '<img src="'+src+'" class="img-responsive" alt=" " />' +
                            '</figure>' +
                            '</a>' +
                            '</div>' +
                            '</li>';
                        $('.gallery-img-sec').append(factory);
                    }
                });
                loadVehicle();
            }
        });
    }

    function loadVehicle(){
        var wedding_folder = "images/gallery/vehicle/";
        $.ajax({
            url : wedding_folder,
            success: function (wedding_data) {
                $(wedding_data).find("a").attr("href", function (i, val) {
                    if( val.match(/\.(jpe?g|png)$/) ) {
                        var src = val;
                        var vehicle = '<li class="main-item grid-sizer all vehicle">'+
                            '<div class="grid">' +
                            '<a href="'+src+'" class="lsb-preview" data-lsb-group="header">' +
                            '<figure class="effect-winston">' +
                            '<img src="'+src+'" class="img-responsive" alt=" " />' +
                            '</figure>' +
                            '</a>' +
                            '</div>' +
                            '</li>';
                        $('.gallery-img-sec').append(vehicle);
                    }
                });
                loadWorker();
            }
        });
    }

    function loadWorker(){
        var wedding_folder = "images/gallery/worker/";
        $.ajax({
            url : wedding_folder,
            success: function (wedding_data) {
                $(wedding_data).find("a").attr("href", function (i, val) {
                    if( val.match(/\.(jpe?g|png)$/) ) {
                        var src = val;
                        var worker = '<li class="main-item grid-sizer all worker">'+
                            '<div class="grid">' +
                            '<a href="'+src+'" class="lsb-preview" data-lsb-group="header">' +
                            '<figure class="effect-winston">' +
                            '<img src="'+src+'" class="img-responsive" alt=" " />' +
                            '</figure>' +
                            '</a>' +
                            '</div>' +
                            '</li>';
                        $('.gallery-img-sec').append(worker);
                    }
                });
                loadCertification();
            }
        });
    }

    function loadCertification(){
        var wedding_folder = "images/gallery/certification/";
        $.ajax({
            url : wedding_folder,
            success: function (wedding_data) {
                $(wedding_data).find("a").attr("href", function (i, val) {
                    if( val.match(/\.(jpe?g|png)$/) ) {
                        var src = val;
                        var certification = '<li class="main-item grid-sizer all certification">'+
                            '<div class="grid">' +
                            '<a href="'+src+'" class="lsb-preview" data-lsb-group="header">' +
                            '<figure class="effect-winston">' +
                            '<img src="'+src+'" class="img-responsive" alt=" " />' +
                            '</figure>' +
                            '</a>' +
                            '</div>' +
                            '</li>';
                        $('.gallery-img-sec').append(certification);
                    }
                });

                initIsotope();
            }
        });
    }

    function initIsotope(){
        $container = $('.gallery-img-sec').isotope({
            layoutMode : 'packery',
            packery: {
                //gutter: 10
                columnWidth: '.grid-sizer'
            }, //grid-sizer
            itemSelector : '.main-item',
            percentPosition: true
        });
    }

    $(window).load(function(){
        if ($('.gallery-part .gallery-img-sec').length) {
            $('#allButton').css("background", "#ffc811");

            if ($container != undefined){
                $container.isotope('layout');
            }

            $('.gallery-part .tabbing-wrapper button').on('click', function() {

                var filterValue = "." + $(this).attr('data-filter');
                $container.isotope({
                    filter : filterValue
                });
                var fancybox = $(this).attr('data-filter');
                $(filterValue).find('a').attr({
                    'data-fancybox-group' : fancybox
                });

            });
        }
    });

    if ($('.fancybox-button').length) {
        $(".fancybox-button").fancybox({
            prevEffect : 'none',
            nextEffect : 'none',
            closeBtn : true,
            helpers : {
                title : {
                    type : 'inside'
                },
                buttons : {}
            }
        });
    }

    $('.control').on('click', function() {
        $(this).remove();
        var video = '<iframe src="' + $('.video img').attr('data-video') + '"></iframe>'
        $('.video img').after(video);
        return false;
    });
</script>
<!-- js -->

<!-- //main slider-banner -->

<script src="js/lsb.min.js"></script>
<script>
    $(window).load(function() {
        $('.gallery_loader').show();
        // language=JQuery-CSS
        $('.loader_image').hide();
        $.fn.lightspeedBox();
    });
</script>
<!-- //js -->

</body>
</html>
