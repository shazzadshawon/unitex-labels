<!DOCTYPE html>
<html lang="en-US">

<?php require('header_links.php'); ?>
<style id='factoryhub-inline-css' type='text/css'>
    .site-header .logo img {
        width: -999999999px;
        height: -999999999px;
    }

    .topbar {
        background-color: #f7f7f7;
    }

    .header-title {
        background-image: url(images/banner11_b.jpg);
    }

    .site-footer {
        background-color: #04192b;
    }

    .footer-widgets {
        background-color: #04192b;
    }

    .woocommerce form.checkout h3 input {
        top: 15px;
    }
</style>

<body class="page-template page-template-template-fullwidth page-template-template-fullwidth-php page page-id-203  no-sidebar header-sticky hide-topbar-mobile blog-classic header-v1 footer- wpb-js-composer js-comp-ver-5.4.2 vc_responsive">
<div id="page" class="hfeed site">

    <?php require('header_top_white.php'); ?>
    <?php require('header_mid.php'); ?>

    <div class="page-header title-area style-1">
        <div class="header-title ">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-title">Contact Us</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="breadcrumb-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <nav class="breadcrumb">
                            <span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                                <a class="home" href="index.php" itemprop="url">
                                    <span itemprop="title">Home</span>
                                </a>
			                </span><i class="fa fa-angle-right" aria-hidden="true"></i>
                            <span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                                <span itemprop="title">Contact</span>
                            </span>
                        </nav>
                    </div>

                    <?php require('page_header_share.php'); ?>

                </div>
            </div>
        </div>
    </div>

    <div id="content" class="site-content">

        <div class="container-fluid">
            <div class="row">

                <div class="vc_row wpb_row vc_row-fluid vc_custom_1490257926483">
                    <div class="container">
                        <div class="row">
                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="fh-section-title clearfix  text-center style-1" style="margin-bottom: 30px;">
                                            <h2>Get In Touch With Us</h2>
                                        </div>
                                        <div class="vc_row wpb_row vc_inner vc_row-fluid vc_custom_1490257810426">
                                            <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-3 vc_col-md-3">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="fh-contact-box ">
                                                            <div class="contact-box-title"><h3>Visit Our Office</h3></div>
                                                            <div class="info"><p>
                                                                    <span class="info-title">Address: </span>
                                                                    <span class="info-details">B-143, Road: 22, Mohakhali DOHS</span>
                                                                </p>
                                                                <p>
                                                                    <span class="info-title">City: </span>
                                                                    <span class="info-details">Dhaka-1206</span>
                                                                </p>
                                                                <p>
                                                                    <span class="info-title">Country: </span>
                                                                    <span class="info-details">Bangladesh</span>
                                                                </p></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-3 vc_col-md-3">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="fh-contact-box ">
                                                            <div class="contact-box-title"><h3>Visit Our Factory</h3></div>
                                                            <div class="info"><p>
                                                                    <span class="info-title">Address: </span>
                                                                    <span class="info-details">Bara Rangamatia, Ashulia</span>
                                                                </p>
                                                                <p>
                                                                    <span class="info-title">City: </span>
                                                                    <span class="info-details">Savar, Dhaka</span>
                                                                </p>
                                                                <p>
                                                                    <span class="info-title">Country: </span>
                                                                    <span class="info-details">Bangladesh</span>
                                                                </p></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-3 vc_col-md-3">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="fh-contact-box ">
                                                            <div class="contact-box-title"><h3>24/7 Quick Contact</h3>
                                                            </div>
                                                            <div class="info"><p>
                                                                    <span class="info-title">Phone: </span> <br>
                                                                    <span class="info-details">Office: +880 1713 175906</span>
                                                                    <span class="info-details">Factory: +880 1678 440802</span>
                                                                </p>
                                                                <p>
                                                                    <span class="info-title">Email: </span>
                                                                    <span class="info-details">info@unitex-bd.net </span>
                                                                </p>
                                                                <p>
                                                                    <span class="info-title">Website: </span>
                                                                    <span class="info-details">www.unitex-bd.net</span>
                                                                </p></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-3 vc_col-md-3">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="fh-contact-box ">
                                                            <div class="contact-box-title"><h3>Working Hours</h3></div>
                                                            <div class="info"><p>
                                                                    <span class="info-title">Mon - Fri Day: </span>
                                                                    <span class="info-details">9.00am to 18.00pm</span>
                                                                </p>
                                                                <p>
                                                                    <span class="info-title">Saturday: </span>
                                                                    <span class="info-details">10.00am to 16.00pm</span>
                                                                </p>
                                                                <p>
                                                                    <span class="info-title">Sunday: </span>
                                                                    <span class="info-details">We are Closed</span>
                                                                </p></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="vc_row wpb_row vc_row-fluid" style="margin-top: 100px;">
                    <div class="container">
                        <div class="row">
                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="vc_separator wpb_content_element vc_separator_align_center vc_sep_width_100 vc_sep_pos_align_center vc_separator_no_text">
                                            <span class="vc_sep_holder vc_sep_holder_l"><span
                                                        style="border-color:#f4f4f4;" class="vc_sep_line"></span></span><span
                                                    class="vc_sep_holder vc_sep_holder_r"><span
                                                        style="border-color:#f4f4f4;" class="vc_sep_line"></span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <?php require('contact_full_form.php'); ?>


                <?php require('contact_locationMap.php'); ?>

            </div> <!-- .row -->
        </div> <!-- .container -->
    </div><!-- #content -->


    <?php require('footer.php'); ?>

</div><!-- #page -->

<?php require('common_modal.php'); ?>
<?php require('menu_mobile.php'); ?>

<script type='text/javascript'>
    /* <![CDATA[ */
    var wpcf7 = {
        "apiSettings": {
            "root": "http:\/\/demo2.steelthemes.com\/factoryhub\/wp-json\/contact-form-7\/v1",
            "namespace": "contact-form-7\/v1"
        }, "recaptcha": {"messages": {"empty": "Please verify that you are not a robot."}}
    };
    /* ]]> */
</script>

<?php require('scripts.php'); ?>
<?php require('contanct_map_scripts.php'); ?>

</body>

</html>
