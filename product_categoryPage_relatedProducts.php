<div id="woocommerce_top_rated_products-2" class="widget woocommerce widget_top_rated_products">
    <h4 class="widget-title">Top Rated Products</h4>
    <ul class="product_list_widget">

        <li>
            <a href="product_detailsPage.php">
                <img width="180" height="180"
                     src="images/products/product1-180x180.png"
                     class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" alt=""
                     srcset="images/products/product1-180x180.png 180w, images/products/product1-180x180.png 150w"
                     sizes="(max-width: 180px) 100vw, 180px"/> <span class="product-title">Product Name</span>
            </a>
            <span class="woocommerce-Price-amount amount">SubTitle</span>
        </li>

        <li>
            <a href="product_detailsPage.php">
                <img width="180" height="180"
                     src="images/products/product1-180x180.png"
                     class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" alt=""
                     srcset="images/products/product1-180x180.png 180w, images/products/product1-180x180.png 150w"
                     sizes="(max-width: 180px) 100vw, 180px"/> <span class="product-title">Product Name</span>
            </a>
            <span class="woocommerce-Price-amount amount">SubTitle</span>
        </li>
        <li>
            <a href="product_detailsPage.php">
                <img width="180" height="180"
                     src="images/products/product1-180x180.png"
                     class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" alt=""
                     srcset="images/products/product1-180x180.png 180w, images/products/product1-180x180.png 150w"
                     sizes="(max-width: 180px) 100vw, 180px"/> <span class="product-title">Product Name</span>
            </a>
            <span class="woocommerce-Price-amount amount">SubTitle</span>
        </li>

    </ul>
</div>