<div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-8 vc_col-md-6">
    <div class="vc_column-inner ">
        <div class="wpb_wrapper">
            <div role="form" class="wpcf7" id="wpcf7-f65-p11-o1" lang="en-US" dir="ltr">
                <div class="screen-reader-response"></div>
                <form action="http://demo2.steelthemes.com/factoryhub/#wpcf7-f65-p11-o1"
                      method="post" class="wpcf7-form" novalidate="novalidate">
                    <div style="display: none;">
                        <input type="hidden" name="_wpcf7" value="65"/>
                        <input type="hidden" name="_wpcf7_version" value="4.9.2"/>
                        <input type="hidden" name="_wpcf7_locale" value="en_US"/>
                        <input type="hidden" name="_wpcf7_unit_tag"
                               value="wpcf7-f65-p11-o1"/>
                        <input type="hidden" name="_wpcf7_container_post" value="11"/>
                    </div>
                    <div class="fh-form fh-form-2">
                        <p class="field first-row"><span
                                class="wpcf7-form-control-wrap first-name"><input
                                    type="text" name="first-name" value="" size="40"
                                    class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                    aria-required="true" aria-invalid="false"
                                    placeholder="First Name*"/></span></p>
                        <p class="field last-row"><span
                                class="wpcf7-form-control-wrap last-name"><input
                                    type="text" name="last-name" value="" size="40"
                                    class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                    aria-required="true" aria-invalid="false"
                                    placeholder="Last Name*"/></span></p>
                        <p class="field first-row"><span
                                class="wpcf7-form-control-wrap your-email"><input
                                    type="email" name="your-email" value="" size="40"
                                    class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
                                    aria-required="true" aria-invalid="false"
                                    placeholder="Email*"/></span></p>
                        <!--<p class="field last-row"><span
                                class="wpcf7-form-control-wrap menu-170"><select
                                    name="menu-170"
                                    class="wpcf7-form-control wpcf7-select"
                                    aria-invalid="false"><option
                                        value="Select Your Services">Select Your Services</option><option
                                        value="Service 1">Service 1</option><option
                                        value="Service 2">Service 2</option></select></span>
                        </p>-->
                        <p class="field single-field"><span
                                class="wpcf7-form-control-wrap your-message"><textarea
                                    name="your-message" cols="40" rows="10"
                                    class="wpcf7-form-control wpcf7-textarea"
                                    aria-invalid="false"
                                    placeholder="Other Requirements..."></textarea></span>
                        </p>
                        <p class="field submit"><input type="submit" value="Submit"
                                                       class="wpcf7-form-control wpcf7-submit fh-btn"/>
                        </p>
                    </div>
                    <div class="wpcf7-response-output wpcf7-display-none"></div>
                </form>
            </div>
        </div>
    </div>
</div>