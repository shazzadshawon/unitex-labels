<button id="allButton" title="All" data-placement="left" data-toggle="tooltip" class="btn btn-default" data-filter="all" type="button">
    ALL
</button>
<button title="MACHINERIES" data-placement="bottom" data-toggle="tooltip" class="btn btn-default" data-filter="machineries" type="button">
    Machineries Photos
</button>
<button title="FACTORY" data-placement="right" data-toggle="tooltip" class="btn btn-default" data-filter="factory" type="button">
    Factory Photos
</button>
<button title="PRODUCTS" data-placement="top" data-toggle="tooltip" class="btn btn-default" data-filter="products" type="button">
    Products Photos
</button>
<button title="VEHICLE" data-placement="right" data-toggle="tooltip" class="btn btn-default" data-filter="vehicle" type="button">
    Vehicle Photos
</button>
<button title="WORKER" data-placement="right" data-toggle="tooltip" class="btn btn-default" data-filter="worker" type="button">
    Worker Photos
</button>
<button title="AWARDS" data-placement="right" data-toggle="tooltip" class="btn btn-default" data-filter="certification" type="button">
    Certification &amp; Awards
</button>