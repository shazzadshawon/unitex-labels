<div class="testi-item ">
    <div class="testi-content" style="border-color: rgba(86, 86, 86, 0.11) !important;">
        <div class="info clearfix">
            <img width="80" height="80"
                 src="images/80_80.png"
                 class="attachment-factoryhub-testimonial-thumb size-factoryhub-testimonial-thumb wp-post-image"
                 alt=""
                 sizes="(max-width: 80px) 100vw, 80px"
                 style="border-radius: 10px; !important;"/>
            <h4 class="testi-name" style="color: #282828;!important;">Award/Certification Name</h4>
            <span class="testi-job">Award Source</span>
        </div>
        <div class="testi-des" style="color: #242424;">Description about the award / certifications. Short Description.
        </div>
        <i class="fa fa-quote-right"
           aria-hidden="true"></i>
    </div>
</div>