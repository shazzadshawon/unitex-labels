<li class="post-1885 product type-product status-publish has-post-thumbnail product_cat-agriculture product_tag-books product_tag-classic product_tag-engines  instock featured shipping-taxable purchasable product-type-simple col-sm-6 col-xs-6 col-md-4">
    <div class="product-inner">
        <a href="product_detailsPage.php" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
            <div class="product-header"><a rel="nofollow"
                                           href="product_detailsPage.php"
                                           data-quantity="1" data-product_id="1885"
                                           data-product_sku=""
                                           class="button product_type_simple add_to_cart_button">View Details</a><img width="270" height="270"
                                          src="images/products/270_270.png"
                                          class="attachment-shop_catalog size-shop_catalog wp-post-image"
                                          alt=""
                                          srcset="images/products/270_270.png 270w, images/products/270_270.png 150w"
                                          sizes="(max-width: 270px) 100vw, 270px"/></div>
            <h2 class="woocommerce-loop-product__title">Product Name</h2>
            <span class="price"><span class="woocommerce-Price-amount amount">
                                    <span class="woocommerce-Price-currencySymbol"></span>Short Descripee</span>
        </a>
    </div>
</li>