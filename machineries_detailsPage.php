<!DOCTYPE html>
<html lang="en-US">

<?php require('header_links.php'); ?>
<style id='factoryhub-inline-css' type='text/css'>
    .site-header .logo img {
        width: -999999999px;
        height: -999999999px;
    }

    .topbar {
        background-color: #f7f7f7;
    }

    .header-title {
        background-image: url(images/banner4_b.jpg);
    }

    .site-footer {
        background-color: #04192b;
    }

    .footer-widgets {
        background-color: #04192b;
    }

    .woocommerce form.checkout h3 input {
        top: 15px;
    }
</style>

<body class="project-template-default single single-project postid-176  no-sidebar header-sticky hide-topbar-mobile blog-classic header-v1 footer- wpb-js-composer js-comp-ver-5.4.2 vc_responsive">
<div id="page" class="hfeed site">

    <?php require('header_top_white.php'); ?>
    <?php require('header_mid.php'); ?>

    <div class="page-header title-area style-1">
        <div class="header-title ">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-title">Machine Name</h1></div>
                </div>
            </div>
        </div>
        <div class="breadcrumb-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <nav class="breadcrumb">
                            <span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
				                <a class="home" href="index.php" itemprop="url"><span itemprop="title">Home</span></a>
			                </span>
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                            <span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
			                    <a href="machineries_categoryPage.php" itemprop="url">
                                    <span itemprop="title">Machine Category Name</span></a>
                            </span>
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                            <span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                                <span itemprop="title">Machine Name</span>
                            </span>
                        </nav>
                    </div>

                    <?php require('page_header_share.php'); ?>

                </div>
            </div>
        </div>
    </div>
    <div id="content" class="site-content">

        <div class="container">
            <div class="row">

                <div id="primary" class="content-area col-md-12">
                    <div class="site-main">

                        <div class="single-project post-176 project type-project status-publish has-post-thumbnail hentry project_category-agriculture project_category-power ">
                            <div class="row">
                                <div class="entry-thumbnail col-md-12 col-sm-12 col-xs-12">
                                    <div class="item">
                                        <a class="fancybox" rel="portfolio-gallery"><img
                                                    src="images/machine_details/c5-1170x500.jpg"
                                                    alt="c5.jpg"/></a>
                                    </div>

                                    <?php include('machine_details_slide.php'); ?>
                                    <?php include('machine_details_slide.php'); ?>
                                    <?php include('machine_details_slide.php'); ?>
                                    <?php include('machine_details_slide.php'); ?>
                                    <?php include('machine_details_slide.php'); ?>
                                    <?php include('machine_details_slide.php'); ?>

                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <h2 class="single-project-title">Machine Name</h2></div>

                                <div class="entry-content col-md-9 col-sm-12 col-xs-12">
                                    <p>Machine Details.Machine Details.Machine Details.Machine Details.Machine Details.Machine Details.Machine Details.Machine Details.Machine Details.Machine Details.Machine Details.Machine Details.Machine Details.Machine Details.Machine Details.Machine Details.Machine Details.Machine Details.Machine Details.Machine Details.</p>
                                    <p>Machine Details.Machine Details.Machine Details.Machine Details.Machine Details.Machine Details.Machine Details.Machine Details.Machine Details.Machine Details.Machine Details.Machine Details.Machine Details.Machine Details.Machine Details.Machine Details.Machine Details.Machine Details.Machine Details.Machine Details.</p>
                                    <p>Machine Details.Machine Details.Machine Details.Machine Details.Machine Details.Machine Details.Machine Details.Machine Details.Machine Details.Machine Details.Machine Details.Machine Details.Machine Details.Machine Details.Machine Details.Machine Details.Machine Details.Machine Details.Machine Details.Machine Details.</p>

                                    <div class="project-socials">
                                        <div class="socials">
                                            <a href="https://facebook.com/" target="_blank">
                                                <i class="fa fa-facebook"></i></a><a href="https://twitter.com/" target="_blank">
                                                <i class="fa fa-twitter"></i></a><a href="https://dribbble.com/" target="_blank">
                                                <i class="fa fa-dribbble"></i>
                                            </a>
                                            <a href="https://www.skype.com/en/" target="_blank">
                                                <i class="fa fa-skype"></i>
                                            </a>
                                            <a href="https://plus.google.com/" target="_blank">
                                                <i class="fa fa-google-plus"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>

                                <div class="metas col-md-3 col-sm-12 col-xs-12">
                                    <div class="meta cat">
                                        <h4>Category :</h4>
                                        <a href="machineries_categoryPage.php" class="cat-project">Machine Category</a>
                                    </div>

                                    <div class="meta client">
                                        <h4>Client :</h4>
                                        Client Name
                                    </div>

                                    <div class="meta date">
                                        <h4>Date :</h4>
                                        April 21, 2017
                                    </div>
                                </div>
                            </div>
                        </div>


                        <nav class="navigation portfolio-navigation">
                            <div class="container">
                                <div class="nav-links clearfix">
                                    <div class="nav-previous">
                                        <a href="machineries_detailsPage.php" rel="prev"><span class="meta-nav left"><i class="fa fa-chevron-left" aria-hidden="true"></i></span>Prev</a>
                                    </div>
                                    <a class="portfolio-link" href="machineries_categoryPage.php"><i class="fa fa-th" aria-hidden="true"></i></a>
                                    <div class="nav-next">
                                        <a href="machineries_detailsPage.php" rel="next">Next<span class="meta-nav right"><i class="fa fa-chevron-right" aria-hidden="true"></i></span></a>
                                    </div>
                                </div><!-- .nav-links -->
                            </div>
                        </nav><!-- .navigation -->

                    </div>
                    <!-- #content -->
                </div><!-- #primary -->
            </div> <!-- .row -->
        </div> <!-- .container -->
    </div><!-- #content -->


    <?php require('footer.php'); ?>

</div><!-- #page -->

<div id="modal" class="modal fade" tabindex="-1" aria-hidden="true">
    <div class="item-detail">
        <div class="modal-dialog woocommerce">
            <div class="modal-content product">
                <div class="modal-header">
                    <button type="button" class="close fh-close-modal" data-dismiss="modal">&#215;<span
                                class="sr-only"></span></button>
                </div>
                <div class="modal-body"></div>
            </div>
        </div>
    </div>
</div>
<div class="primary-mobile-nav" id="primary-mobile-nav" role="navigation">
    <a href="#" class="close-canvas-mobile-panel">
        &#215;
    </a>
    <ul id="menu-primary-menu" class="menu">
        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-147"><a
                    href="#">Home</a>
            <ul class="sub-menu">
                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2666"><a
                            href="../../index5cc2.html?header_layout=v1">Home Page 1</a></li>
                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2667"><a
                            href="../../home-page-2/index4724.html?header_layout=v2">Home Page 2</a></li>
                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2668"><a
                            href="../../home-page-3/indexe431.html?header_layout=v3">Home Page 3</a></li>
                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2669"><a
                            href="../../home-page-4/indexbef1.html?header_layout=v4">Home Page 4</a></li>
            </ul>
        </li>
        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-216"><a
                    href="#">About Us</a>
            <ul class="sub-menu">
                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-217"><a
                            href="../../about-us/index.html">About Us</a></li>
                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-218"><a
                            href="../../about-us-2/index.html">About Us 2</a></li>
            </ul>
        </li>
        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-140"><a
                    href="../../services/index.html">Services</a>
            <ul class="sub-menu">
                <li class="menu-item menu-item-type-post_type menu-item-object-service menu-item-146"><a
                            href="../../service/agriculture-processing/index.html">Agriculture Processing</a></li>
                <li class="menu-item menu-item-type-post_type menu-item-object-service menu-item-145"><a
                            href="../../service/chemical-engineering/index.html">Chemical Engineering</a></li>
                <li class="menu-item menu-item-type-post_type menu-item-object-service menu-item-144"><a
                            href="../../service/material-engineering/index.html">Material Engineering</a></li>
                <li class="menu-item menu-item-type-post_type menu-item-object-service menu-item-143"><a
                            href="../../service/mechanical-engineering/index.html">Mechanical Engineering</a></li>
                <li class="menu-item menu-item-type-post_type menu-item-object-service menu-item-142"><a
                            href="../../service/petroleum-and-gas/index.html">Petroleum and Gas</a></li>
                <li class="menu-item menu-item-type-post_type menu-item-object-service menu-item-141"><a
                            href="../../service/power-and-energy/index.html">Power and Energy</a></li>
            </ul>
        </li>
        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-139"><a
                    href="../../projects/index.html">Projects</a>
            <ul class="sub-menu">
                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-2672">
                    <a href="#">Projects Fullwidth</a>
                    <ul class="sub-menu">
                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2680"><a
                                    href="../index0497.html?project_layout=full_width&amp;project_columns=2">2 Columns
                                Fullwidth</a></li>
                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2679"><a
                                    href="../indexee4a.html?project_layout=full_width&amp;project_columns=3">3 Columns
                                Fullwidth</a></li>
                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2678"><a
                                    href="../indexdc51.html?project_layout=full_width&amp;project_columns=4">4 Columns
                                Fullwidth</a></li>
                    </ul>
                </li>
                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-2671">
                    <a href="#">Projects Grid View</a>
                    <ul class="sub-menu">
                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2675"><a
                                    href="../index6ed1.html?project_layout=grid&amp;project_columns=2&amp;project_nav_type=link">2
                                Columns Grid View</a></li>
                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2676"><a
                                    href="../indexc1fe.html?project_layout=grid&amp;project_columns=3&amp;project_nav_type=link">3
                                Columns Grid View</a></li>
                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2677"><a
                                    href="../index006d.html?project_layout=grid&amp;project_columns=4&amp;project_nav_type=link">4
                                Columns Grid View</a></li>
                    </ul>
                </li>
                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2673"><a
                            href="../index63ee.html?layout_project=single-right&amp;project_layout=grid&amp;project_columns=2&amp;project_nav_type=link">Projects
                        With Sidebar</a></li>
                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2674"><a
                            href="../london-wind-energy-plant/index.html">Single Project</a></li>
            </ul>
        </li>
        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-215"><a
                    href="#">Features</a>
            <ul class="sub-menu">
                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-220"><a
                            href="../../faq/index.html">FAQ</a></li>
                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-223"><a
                            href="../../testimonials/index.html">Testimonials</a></li>
                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-222"><a
                            href="../../pricing-table/index.html">Pricing Table</a></li>
                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-221"><a
                            href="../../our-team/index.html">Our Team</a></li>
                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2642"><a
                            href="../../404/index.html">404 Page</a></li>
            </ul>
        </li>
        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-2670"><a
                    href="../../blog/index.html">New</a>
            <ul class="sub-menu">
                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-2683">
                    <a href="#">Blog Grid View</a>
                    <ul class="sub-menu">
                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2684"><a
                                    href="../../blog/index77e2.html?layout_default=no-sidebar&amp;blog_layout=grid&amp;blog_grid_columns=2">2
                                Columns Grid View</a></li>
                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2685"><a
                                    href="../../blog/indexebf9.html?layout_default=no-sidebar&amp;blog_layout=grid&amp;blog_grid_columns=3">3
                                Columns Grid View</a></li>
                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2686"><a
                                    href="../../blog/indexe2d4.html?layout_default=no-sidebar&amp;blog_layout=grid&amp;blog_grid_columns=4">4
                                Columns Grid View</a></li>
                    </ul>
                </li>
                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2682"><a
                            href="../../blog/index32bb.html?layout_default=single-right&amp;blog_layout=classic">Blog
                        With Sidebar</a></li>
                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2681"><a
                            href="../../2017/04/19/materials-manufacturing-service-engineers/index12cf.html?layout_default=single-right&amp;show_author_box=1">Blog
                        Single Post</a></li>
            </ul>
        </li>
        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-134"><a
                    href="../../shop/index.html">Shop</a>
            <ul class="sub-menu">
                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2687"><a
                            href="../../shop/index.html">Our Products</a></li>
                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2689"><a
                            href="../../product/claw-hamer/index.html">Single Product</a></li>
                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2688"><a
                            href="../../cart/index.html">Shopping Cart</a></li>
                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-132"><a
                            href="../../checkout/index.html">Checkout</a></li>
                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-131"><a
                            href="../../my-account/index.html">My account</a></li>
            </ul>
        </li>
        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-219"><a
                    href="../../contact/index.html">Contact</a></li>
    </ul>


    <div class="extra-item menu-item-text"><i class="flaticon-technology-1"></i>
        <div class="extra-text">
            <p class="fh-text">Toll Free Number</p>
            <p class="number">+(32) 978 654 321</p>
        </div>
    </div>
</div>
<div id="off-canvas-layer" class="off-canvas-layer"></div>
<a id="scroll-top" class="backtotop" href="#page-top">
    <i class="fa fa-angle-up"></i>
</a>

<script type="text/javascript">(function () {
        function addEventListener(element, event, handler) {
            if (element.addEventListener) {
                element.addEventListener(event, handler, false);
            } else if (element.attachEvent) {
                element.attachEvent('on' + event, handler);
            }
        }

        function maybePrefixUrlField() {
            if (this.value.trim() !== '' && this.value.indexOf('http') !== 0) {
                this.value = "http://" + this.value;
            }
        }

        var urlFields = document.querySelectorAll('.mc4wp-form input[type="url"]');
        if (urlFields && urlFields.length > 0) {
            for (var j = 0; j < urlFields.length; j++) {
                addEventListener(urlFields[j], 'blur', maybePrefixUrlField);
            }
        }
        /* test if browser supports date fields */
        var testInput = document.createElement('input');
        testInput.setAttribute('type', 'date');
        if (testInput.type !== 'date') {

            /* add placeholder & pattern to all date fields */
            var dateFields = document.querySelectorAll('.mc4wp-form input[type="date"]');
            for (var i = 0; i < dateFields.length; i++) {
                if (!dateFields[i].placeholder) {
                    dateFields[i].placeholder = 'YYYY-MM-DD';
                }
                if (!dateFields[i].pattern) {
                    dateFields[i].pattern = '[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])';
                }
            }
        }

    })();</script>

<?php require('scripts.php'); ?>

</body>

</html>
