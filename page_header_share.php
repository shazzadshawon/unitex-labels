<div class="share col-md-6 col-sm-6 col-xs-12">
    <div class="share-title">
        <i class="fa fa-share-alt" aria-hidden="true"></i>Share
    </div>
    <ul class="socials-share">
        <li>
            <a target="_blank" class="share-facebook social"
               href="http://www.facebook.com">
                <i class="fa fa-facebook"></i>
            </a>
        </li>
        <li>
            <a class="share-twitter social"
               href="http://twitter.com"
               target="_blank">
                <i class="fa fa-twitter"></i>
            </a>
        </li>
        <li>
            <a target="_blank" class="share-google-plus social"
               href="https://plus.google.com"><i
                    class="fa fa-google-plus"></i>
            </a>
        </li>
        <li>
            <a class="share-linkedin social"
               href="https://www.linkedin.com"
               target="_blank">
                <i class="fa fa-linkedin"></i>
            </a>
        </li>
    </ul>
</div>