<!DOCTYPE html>
<html lang="en-US">

<?php require('header_links.php'); ?>
<style id='factoryhub-inline-css' type='text/css'>
    .site-header .logo img {
        width: -999999999px;
        height: -999999999px;
    }

    .topbar {
        background-color: #f7f7f7;
    }

    .header-title {
        background-image: url(images/banner5_b.jpg);
    }

    .site-footer {
        background-color: #04192b;
    }

    .footer-widgets {
        background-color: #04192b;
    }

    .woocommerce form.checkout h3 input {
        top: 15px;
    }
</style>

<body class="page-template page-template-template-fullwidth page-template-template-fullwidth-php page page-id-196  no-sidebar header-sticky hide-topbar-mobile blog-classic header-v1 footer- wpb-js-composer js-comp-ver-5.4.2 vc_responsive">
<div id="page" class="hfeed site">

    <?php require('header_top_white.php'); ?>
    <?php require('header_mid.php'); ?>


    <div class="page-header title-area style-2">
        <div class="header-title">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="subtitle">About Us</div>
                        <h1 class="page-title">Company Profile <br>
                            Board &amp; Management</h1>
                        <div class="page-button-link">
                            <a href="contact.php">Get In Touch with us Today</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="breadcrumb-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <nav class="breadcrumb">
                            <span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
				                <a class="home" href="index.php" itemprop="url"><span itemprop="title">Home</span></a>
			                </span>
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                            <span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                                <span itemprop="title">About Us-Company Profile &amp; Board and Management</span>
                            </span>
                        </nav>
                    </div>

                    <?php require('page_header_share.php'); ?>

                </div>
            </div>
        </div>
    </div>
    <div id="content" class="site-content">

        <div id="companyProfile" class="container-fluid">
            <div class="row">

                <div class="vc_row wpb_row vc_row-fluid vc_custom_1490586960070">
                    <div class="container">
                        <div class="row">
                            <div class="wpb_column vc_column_container vc_col-sm-6">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="wpb_single_image wpb_content_element vc_align_left">

                                            <figure class="wpb_wrapper vc_figure">
                                                <div class="vc_single_image-wrapper   vc_box_border_grey"><img
                                                            class="vc_single_image-img "
                                                            src="images/about-1.jpg"
                                                            width="570" height="345" alt="about-1" title="about-1"/>
                                                </div>
                                            </figure>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="wpb_column vc_column_container vc_col-sm-6" style="border-left: 4px solid #e0e0e0">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="wpb_text_column wpb_content_element  vc_custom_1494386579181">
                                            <div class="wpb_wrapper">
                                                <h2 style="font-size: 30px; font-weight: 600; color: #222; font-family: 'Open Sans', sans-serif; margin-top: 0; margin-bottom: 10px;">
                                                    Company Profile</h2>
                                                <p>Our goal is to provide the customized solution for apparel brands and retailers. We are able to provide services all over the world. Our extensive experience in the industry has helped acquire knowledge and information to design products and services that best suites their requirements of our clients. It is the vision of Unitex Labels Ltd. to grow along with the customers through minimizing cost, lead time and mutually beneficial commitments. We have our own designers for woven label, printed labels & offset printing as well as all required hard wares and soft wares. We believe in working closely with our clients to understand their real needs to designs services accordingly and align ourselves as strategic partner.</p>
                                                <h3 class="main-color"
                                                    style="font-size: 20px; font-weight: 600; font-family: 'Open Sans', sans-serif; margin-top: 15px; margin-bottom: 15px;">
                                                    Trusted Suppliers</h3>
                                                <p>Our goal is to provide the customized solution for apparel brands and retailers. We are able to provide services all over the world. Our extensive experience in the industry has helped acquire knowledge and information to design products and services that best suites their requirements of our clients. It is the vision of Unitex Labels Ltd. to grow along with the customers through minimizing cost, lead time and mutually beneficial commitments. We have our own designers for woven label, printed labels & offset printing as well as all required hard wares and soft wares. We believe in working closely with our clients to understand their real needs to designs services accordingly and align ourselves as strategic partner.</p>

                                            </div>
                                        </div>
                                        <div class="vc_btn3-container  fh-button vc_btn3-inline">
                                            <a style="color:#222222;"
                                               class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-rounded vc_btn3-style-custom vc_btn3-icon-right"
                                               href="contact.php" title="Get In Touch">Get In Touch <i class="vc_btn3-icon fa fa-angle-right"></i></a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="vc_row wpb_row vc_row-fluid">
                    <div class="container">
                        <div class="row">
                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="vc_separator wpb_content_element vc_separator_align_center vc_sep_width_100 vc_sep_pos_align_center vc_separator_no_text">
                                            <span class="vc_sep_holder vc_sep_holder_l"><span
                                                        style="border-color:#f4f4f4;" class="vc_sep_line"></span></span><span
                                                    class="vc_sep_holder vc_sep_holder_r"><span
                                                        style="border-color:#f4f4f4;" class="vc_sep_line"></span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="vc_row wpb_row vc_row-fluid vc_custom_1490597759156">
                    <div class="container">
                        <div class="row">

                            <?php include('about_profile_slide.php'); ?>
                            <?php include('about_profile_slide.php'); ?>
                            <?php include('about_profile_slide.php'); ?>
                            <?php include('about_profile_slide.php'); ?>
                            <?php include('about_profile_slide.php'); ?>

                        </div>
                    </div>
                </div>

                <?php require('company_about_banner.php'); ?>

                <div class="vc_row-full-width vc_clearfix"></div>

                <div id="boardandmanagement" class="vc_row wpb_row vc_row-fluid vc_custom_1490597375444" style="margin-top: 70px;">
                    <div class="container">
                        <div class="row">
                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="fh-section-title clearfix  text-center style-1">
                                            <h2>Board and Management</h2>
                                        </div>
                                        <div class="vc_row wpb_row vc_inner vc_row-fluid vc_custom_1490588863772">

                                            <?php include('prodile_boardManagement_slide.php'); ?>
                                            <?php include('prodile_boardManagement_slide.php'); ?>
                                            <?php include('prodile_boardManagement_slide.php'); ?>
                                            <?php include('prodile_boardManagement_slide.php'); ?>
                                            <?php include('prodile_boardManagement_slide.php'); ?>
                                            <?php include('prodile_boardManagement_slide.php'); ?>
                                            <?php include('prodile_boardManagement_slide.php'); ?>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div> <!-- .row -->
        </div> <!-- .container -->
    </div><!-- #content -->


    <?php require('footer.php'); ?>

</div><!-- #page -->

<?php require('common_modal.php'); ?>
<?php require('menu_mobile.php'); ?>

<link rel='stylesheet' id='font-awesome-css'
      href='wp-content/plugins/js_composer/assets/lib/bower/font-awesome/css/font-awesome.min7661.css?ver=5.4.2'
      type='text/css' media='all'/>


<?php require('scripts.php'); ?>

</body>
</html>
