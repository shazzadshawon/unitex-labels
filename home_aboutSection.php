<div class="vc_row wpb_row vc_row-fluid vc_custom_1493883474048">
    <div class="container">
        <div class="row">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner ">
                    <div class="wpb_wrapper">
                        <div class="fh-section-title clearfix  text-center style-1">
                            <h2>About Us</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="vc_row wpb_row vc_row-fluid vc_custom_1493883483824">
    <div class="container">
        <div class="row">

            <!--LEFT POSTION-->
            <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-4 vc_col-md-4">
                <div class="vc_column-inner ">
                    <div class="wpb_wrapper">
                        <div class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1492678998025">

                            <figure class="wpb_wrapper vc_figure">
                                <div class="vc_single_image-wrapper vc_box_border_grey">
                                    <img
                                        width="370" height="200"
                                        src="images/canvasTape.jpg"
                                        class="vc_single_image-img attachment-full" alt=""
                                        srcset="images/canvasTape.jpg, images/canvasTape.jpg 300w"
                                        sizes="(max-width: 370px) 100vw, 370px"
                                        style="border-radius: 6px;"/>
                                </div>
                            </figure>
                        </div>

                        <div class="wpb_text_column wpb_content_element  vc_custom_1492574283334">
                            <div class="wpb_wrapper">
                                <p>Our goal is to provide the customized solution for apparel brands and retailers. We are able to provide services all over the world. Our extensive experience in the industry has helped acquire knowledge and information to design products and services that best suites their requirements of our clients. It is the vision of Unitex Labels Ltd. to grow along with the customers through minimizing cost, lead time and mutually beneficial commitments. We have our own designers for woven label, printed labels & offset printing as well as all required hard wares and soft wares. We believe in working closely with our clients to understand their real needs to designs services accordingly and align ourselves as strategic partner.</p>

                            </div>
                        </div>
                        <div class="vc_btn3-container  about-btn vc_btn3-inline">
                            <a class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-round vc_btn3-style-modern vc_btn3-color-white" style="border-color: #ffc811 !important;"
                               href="about_companyProfile.php" title="Read More">Read More</a></div>
                    </div>
                </div>
            </div>


            <!--MIDDLE PORION-->
            <div class="fh_vc_list-service wpb_column vc_column_container vc_col-sm-12 vc_col-lg-4 vc_col-md-4 vc_col-has-fill">
                <div class="vc_column-inner vc_custom_1494302424589">
                    <div class="wpb_wrapper">
                        <div class="wpb_text_column wpb_content_element ">
                            <div class="wpb_wrapper">
                                <h4 style="font-size: 20px; font-weight: 600; color: #222; margin-top: 0; margin-bottom: 25px;">Why With Us?</h4>
                                <p style="color: #212121;">Our goal is to provide the customized solution for apparel brands and retailers. We are able to provide services all over the world. Our extensive experience in the industry has helped acquire knowledge and information to design products and services that best suites their requirements of our clients. </p>

                            </div>
                        </div>
                        <div class="fh-service-list ">
                            <ul>
                                <li>
                                    <a href="whyWithus.php#1"><i class="fa fa-check-circle-o"></i>Quality Assurance</a>
                                </li>
                                <li>
                                    <a href="whyWithus.php#2"><i class="fa fa-check-circle-o"></i>Sustainability</a>
                                </li>
                                <li>
                                    <a href="whyWithus.php#3"><i class="fa fa-check-circle-o"></i>Green Technology</a>
                                </li>
                                <li>
                                    <a href="whyWithus.php#4"><i class="fa fa-check-circle-o"></i>Health & Safety</a>
                                </li>
                                <li>
                                    <a href="whyWithus.php#5"><i class="fa fa-check-circle-o"></i>Code of Conduct</a>
                                </li>
                                <li>
                                    <a href="whyWithus.php#6"><i class="fa fa-check-circle-o"></i>Workers Benefits</a>
                                </li>
                                <li>
                                    <a href="whyWithus.php#7"><i class="fa fa-check-circle-o"></i>Compliance</a>
                                </li>
                                <li>
                                    <a href="whyWithus.php#8"><i class="fa fa-check-circle-o"></i>Transportation</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>


            <!--RIGHT PORTION-->
            <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-4 vc_col-md-4">
                <div class="vc_column-inner ">
                    <div class="wpb_wrapper">
                        <div class="vc_tta-container" data-vc-action="collapse">
                            <div class="vc_general vc_tta vc_tta-accordion vc_tta-color-grey vc_tta-style-classic vc_tta-shape-square vc_tta-o-shape-group vc_tta-gap-10 vc_tta-controls-align-left fh-accordion fh-accordion">
                                <div class="vc_tta-panels-container">
                                    <div class="vc_tta-panels">

                                        <div class="vc_tta-panel vc_active"
                                             id="homea1"
                                             data-vc-content=".vc_tta-panel-body">
                                            <div class="vc_tta-panel-heading"><h4
                                                    class="vc_tta-panel-title"><a
                                                        href="#homea1"
                                                        data-vc-accordion
                                                        data-vc-container=".vc_tta-container"><span
                                                            class="vc_tta-title-text">We are Passionate</span><i
                                                            class="vc_tta-icon fa fa-angle-down"></i></a>
                                                </h4></div>
                                            <div class="vc_tta-panel-body">
                                                <div class="wpb_text_column wpb_content_element ">
                                                    <div class="wpb_wrapper">
                                                        <p>Our goal is to provide the customized solution for apparel brands and retailers. We are able to provide services all over the world. Our extensive experience in the industry has helped acquire knowledge and information to design products and services that best suites their requirements of our clients.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="vc_tta-panel" id="homea2"
                                             data-vc-content=".vc_tta-panel-body">
                                            <div class="vc_tta-panel-heading"><h4
                                                        class="vc_tta-panel-title"><a
                                                            href="#homea2"
                                                            data-vc-accordion
                                                            data-vc-container=".vc_tta-container"><span
                                                                class="vc_tta-title-text">Our Product Range</span><i
                                                                class="vc_tta-icon fa fa-angle-down"></i></a>
                                                </h4></div>
                                            <div class="vc_tta-panel-body">
                                                <div class="wpb_text_column wpb_content_element ">
                                                    <div class="wpb_wrapper">
                                                        <ul>
                                                            <li>Paper Item (Hang Tag, Paper Top, Photo Card, Price Sticker etc)</li>
                                                            <li>Satin Printing Label</li>
                                                            <li>Cotton Printing Label</li>
                                                            <li>Woven Label (Taffeta, Damask, Satin, Both Side Patch Label)</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="vc_tta-panel" id="homea3"
                                             data-vc-content=".vc_tta-panel-body">
                                            <div class="vc_tta-panel-heading"><h4
                                                        class="vc_tta-panel-title"><a
                                                            href="#homea3"
                                                            data-vc-accordion
                                                            data-vc-container=".vc_tta-container"><span
                                                                class="vc_tta-title-text">Our Production Capacity</span><i
                                                                class="vc_tta-icon fa fa-angle-down"></i></a>
                                                </h4></div>
                                            <div class="vc_tta-panel-body">
                                                <div class="wpb_text_column wpb_content_element ">
                                                    <div class="wpb_wrapper">
                                                        <p>Printing Label: 120, 00,000 Pcs per Month</p>
                                                        <p>Screen Printing Label: 80, 00,000 Pcs P/M</p>
                                                        <p>Paper Label: 135, 00,000 Pcs P/M</p>
                                                        <p>Woven Label: 100, 00,000 Pcs P/M</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="vc_tta-panel" id="homea4"
                                             data-vc-content=".vc_tta-panel-body">
                                            <div class="vc_tta-panel-heading"><h4
                                                        class="vc_tta-panel-title"><a
                                                            href="#homea4"
                                                            data-vc-accordion
                                                            data-vc-container=".vc_tta-container"><span
                                                                class="vc_tta-title-text">Our Markets</span><i
                                                                class="vc_tta-icon fa fa-angle-down"></i></a>
                                                </h4></div>
                                            <div class="vc_tta-panel-body">
                                                <div class="wpb_text_column wpb_content_element ">
                                                    <div class="wpb_wrapper">
                                                        <p>U.S.A and Europe</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="vc_tta-panel" id="homea5"
                                             data-vc-content=".vc_tta-panel-body">
                                            <div class="vc_tta-panel-heading"><h4
                                                        class="vc_tta-panel-title"><a
                                                            href="#homea5"
                                                            data-vc-accordion
                                                            data-vc-container=".vc_tta-container"><span
                                                                class="vc_tta-title-text">Our Major Buyers</span><i
                                                                class="vc_tta-icon fa fa-angle-down"></i></a>
                                                </h4></div>
                                            <div class="vc_tta-panel-body">
                                                <div class="wpb_text_column wpb_content_element ">
                                                    <div class="wpb_wrapper">
                                                        <p>Pull & Bear</p>
                                                        <p>Zara</p>
                                                        <p>New-YorkerZara</p>
                                                        <p>Springfild</p>
                                                        <p>Collins</p>
                                                        <p>Mother Care</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="vc_tta-panel" id="homea6"
                                             data-vc-content=".vc_tta-panel-body">
                                            <div class="vc_tta-panel-heading"><h4
                                                        class="vc_tta-panel-title"><a
                                                            href="#homea6"
                                                            data-vc-accordion
                                                            data-vc-container=".vc_tta-container"><span
                                                                class="vc_tta-title-text">Our Sales Turnover</span><i
                                                                class="vc_tta-icon fa fa-angle-down"></i></a>
                                                </h4></div>
                                            <div class="vc_tta-panel-body">
                                                <div class="wpb_text_column wpb_content_element ">
                                                    <div class="wpb_wrapper">
                                                        <p>05 million USD Yearly</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

<!--                                        --><?php //require('home_aboutSection_tabPanel.php'); ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>