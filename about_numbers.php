<div class="vc_row wpb_row vc_row-fluid vc_custom_1490607020527">
    <div class="container">
        <div class="row">
            <div class="wpb_column vc_column_container vc_col-sm-3">
                <div class="vc_column-inner ">
                    <div class="wpb_wrapper">
                        <div class="fh-counter  style-1">
                            <span class="fh-icon"><i class="factory-tool2"></i></span>
                            <div class="counter">
                                <div class="value">742</div>
                                <span>+</span>
                            </div>
                            <h4>Sucessfull Projects</h4>

                        </div>
                    </div>
                </div>
            </div>
            <div class="wpb_column vc_column_container vc_col-sm-3">
                <div class="vc_column-inner ">
                    <div class="wpb_wrapper">
                        <div class="fh-counter  style-1">
                            <span class="fh-icon"><i class="factory-people"></i></span>
                            <div class="counter">
                                <div class="value">100</div>
                                <span>%</span>
                            </div>
                            <h4>Our Satisfied Clients</h4>

                        </div>
                    </div>
                </div>
            </div>
            <div class="wpb_column vc_column_container vc_col-sm-3">
                <div class="vc_column-inner ">
                    <div class="wpb_wrapper">
                        <div class="fh-counter  style-1">
                            <span class="fh-icon"><i class="factory-quality"></i></span>
                            <div class="counter">
                                <div class="value">50</div>
                                <span></span>
                            </div>
                            <h4>Total Wining Awards</h4>

                        </div>
                    </div>
                </div>
            </div>
            <div class="wpb_column vc_column_container vc_col-sm-3">
                <div class="vc_column-inner ">
                    <div class="wpb_wrapper">
                        <div class="fh-counter  style-1">
                            <span class="fh-icon"><i class="flaticon-pencil"></i></span>
                            <div class="counter">
                                <div class="value">247</div>
                                <span></span>
                            </div>
                            <h4>Profesional Workers</h4>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>