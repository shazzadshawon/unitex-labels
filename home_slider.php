<div id="rev_slider_1_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-source="gallery"
     style="margin:0px auto;background:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
    <!-- START REVOLUTION SLIDER 5.4.1 auto mode -->
    <div id="rev_slider_1_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.1">
        <ul>    <!-- SLIDE 1 -->
            <li data-index="rs-1" data-transition="fade" data-slotamount="default" data-hideafterloop="0"
                data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300"
                data-thumb="http://demo2.steelthemes.com/factoryhub/wp-content/uploads/sites/5/revslider/slider_1/1-1-100x50.jpg"
                data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2=""
                data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9=""
                data-param10="" data-description="">
                <!-- MAIN IMAGE -->
                <img src="images/homeSlider/web_cover.jpg" alt="" title="Unitex"
                     data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="14"
                     class="rev-slidebg" data-no-retina>
                <!-- LAYERS -->

                <!-- LAYER NR. 1 -->
                <div class="tp-caption fp_title_layer   tp-resizeme"
                     id="slide-1-layer-1"
                     data-x="['left','left','left','left']" data-hoffset="['560','40','40','40']"
                     data-y="['top','top','top','top']" data-voffset="['160','160','160','190']"
                     data-fontsize="['70','48','48','30']"
                     data-lineheight="['80','60','60','30']"
                     data-width="none"
                     data-height="none"
                     data-whitespace="nowrap"

                     data-type="text"
                     data-responsive_offset="on"

                     data-frames='[{"delay":500,"speed":1000,"frame":"0","from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":700,"frame":"999","to":"y:[-100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"nothing"}]'
                     data-textAlign="['left','left','left','left']"

                     data-paddingtop="[0,0,0,0]"
                     data-paddingright="[0,0,0,0]"
                     data-paddingbottom="[0,0,0,0]"
                     data-paddingleft="[0,0,0,0]"

                     style="z-index: 5; white-space: nowrap; letter-spacing: px;">Welcome to
                </div>

                <!-- LAYER NR. 2 -->
                <div class="tp-caption fp_title_layer tp-resizeme"
                     id="slide-1-layer-5"
                     data-x="['left','left','left','left']" data-hoffset="['560','40','40','40']"
                     data-y="['top','top','top','top']" data-voffset="['245','220','220','240']"
                     data-fontsize="['70','48','48','30']"
                     data-lineheight="['80','60','60','30']"
                     data-width="none"
                     data-height="none"
                     data-whitespace="nowrap"

                     data-type="text"
                     data-responsive_offset="on"

                     data-frames='[{"delay":500,"speed":1000,"frame":"0","from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":700,"frame":"999","to":"y:[-100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"nothing"}]'
                     data-textAlign="['left','left','left','left']"
                     data-paddingtop="[0,0,0,0]"
                     data-paddingright="[0,0,0,0]"
                     data-paddingbottom="[0,0,0,0]"
                     data-paddingleft="[0,0,0,0]"

                     style="z-index: 6; white-space: nowrap; letter-spacing: px;">UNITEX
                </div>

                <!-- LAYER NR. 3 -->
                <div class="tp-caption fp_content_layer   tp-resizeme"
                     id="slide-1-layer-3"
                     data-x="['left','left','left','left']" data-hoffset="['560','40','40','40']"
                     data-y="['top','top','top','top']" data-voffset="['345','300','300','300']"
                     data-fontsize="['24','20','20','18']"
                     data-lineheight="['36','32','32','20']"
                     data-fontweight="['400','700','700','700']"
                     data-color="['rgb(255,200,17)','rgba(250, 192, 18, 1)','rgba(250, 192, 18, 1)','rgba(250, 192, 18, 1)']"
                     data-width="none"
                     data-height="none"
                     data-whitespace="nowrap"

                     data-type="text"
                     data-responsive_offset="on"

                     data-frames='[{"delay":700,"speed":1200,"frame":"0","from":"x:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":900,"frame":"999","to":"x:[-100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
                     data-textAlign="['left','left','left','left']"
                     data-paddingtop="[0,0,0,0]"
                     data-paddingright="[0,0,0,0]"
                     data-paddingbottom="[0,0,0,0]"
                     data-paddingleft="[0,0,0,0]"

                     style="z-index: 7; white-space: nowrap; letter-spacing: px;"> Subtitle
                </div>

                <!-- LAYER NR. 5 -->
                <a href="product_categoryPage.php" class="tp-caption fh_button rev-btn "
                     id="slide-1-layer-8"
                     data-x="['left','left','left','left']" data-hoffset="['560','40','40','40']"
                     data-y="['top','top','top','top']" data-voffset="['450','450','450','450']"
                     data-width="none"
                     data-height="none"
                     data-whitespace="nowrap"

                     data-type="button"
                     data-responsive_offset="on"
                     data-responsive="off"
                     data-frames='[{"delay":900,"speed":700,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":700,"frame":"999","to":"y:50px;opacity:0;","ease":"nothing"},{"frame":"hover","speed":"300","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(255,255,255);bg:rgb(255,200,17);bc:rgb(255,200,17);bw:2 2 2 2;"}]'
                     data-textAlign="['inherit','inherit','inherit','inherit']"
                     data-paddingtop="[12,12,12,12]"
                     data-paddingright="[35,35,35,35]"
                     data-paddingbottom="[12,12,12,12]"
                     data-paddingleft="[35,35,35,35]"

                     style="z-index: 9; white-space: nowrap; letter-spacing: px;background-color:rgba(0, 0, 0, 0);border-color:rgb(255,255,255);outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">
                    View Our Products
                </a>
            </li>
            <!-- SLIDE 2 -->
            <li data-index="rs-2" data-transition="fade" data-slotamount="default" data-hideafterloop="0"
                data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300"
                data-thumb="http://demo2.steelthemes.com/factoryhub/wp-content/uploads/sites/5/revslider/slider_1/1-2-100x50.jpg"
                data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2=""
                data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9=""
                data-param10="" data-description="">
                <!-- MAIN IMAGE -->
                <img src="images/homeSlider/1.jpg" alt="" title="Unitex"
                     data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="14"
                     class="rev-slidebg" data-no-retina>
                <!-- LAYERS -->

                <!-- LAYER NR. 6 -->
                <div class="tp-caption fp_title_layer   tp-resizeme"
                     id="slide-2-layer-1"
                     data-x="['left','left','left','left']" data-hoffset="['0','40','40','40']"
                     data-y="['top','top','top','top']" data-voffset="['160','160','160','190']"
                     data-fontsize="['70','48','48','30']"
                     data-lineheight="['80','60','60','30']"
                     data-width="none"
                     data-height="none"
                     data-whitespace="nowrap"

                     data-type="text"
                     data-responsive_offset="on"

                     data-frames='[{"delay":500,"speed":1000,"frame":"0","from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":700,"frame":"999","to":"y:[-100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"nothing"}]'
                     data-textAlign="['left','left','left','left']"
                     data-paddingtop="[0,0,0,0]"
                     data-paddingright="[0,0,0,0]"
                     data-paddingbottom="[0,0,0,0]"
                     data-paddingleft="[0,0,0,0]"

                     style="z-index: 5; white-space: nowrap; letter-spacing: 1px; color: #c8c8c8;">Welcome To
                </div>

                <!-- LAYER NR. 7 -->
                <div class="tp-caption fp_title_layer   tp-resizeme"
                     id="slide-2-layer-5"
                     data-x="['left','left','left','left']" data-hoffset="['0','40','40','40']"
                     data-y="['top','top','top','top']" data-voffset="['245','220','220','240']"
                     data-fontsize="['70','48','48','30']"
                     data-lineheight="['80','60','60','30']"
                     data-width="none"
                     data-height="none"
                     data-whitespace="nowrap"

                     data-type="text"
                     data-responsive_offset="on"

                     data-frames='[{"delay":500,"speed":1000,"frame":"0","from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":700,"frame":"999","to":"y:[-100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"nothing"}]'
                     data-textAlign="['left','left','left','left']"
                     data-paddingtop="[0,0,0,0]"
                     data-paddingright="[0,0,0,0]"
                     data-paddingbottom="[0,0,0,0]"
                     data-paddingleft="[0,0,0,0]"

                     style="z-index: 6; white-space: nowrap; letter-spacing: 1px; color: #fffef6;">UNITEX
                </div>

                <!-- LAYER NR. 8 -->
                <div class="tp-caption fp_content_layer   tp-resizeme"
                     id="slide-2-layer-3"
                     data-x="['left','left','left','left']" data-hoffset="['0','40','40','40']"
                     data-y="['top','top','top','top']" data-voffset="['345','300','300','300']"
                     data-fontsize="['24','20','20','18']"
                     data-lineheight="['36','32','32','20']"
                     data-fontweight="['400','700','700','700']"
                     data-color="['rgb(255,200,17)','rgba(250, 192, 18, 1)','rgba(250, 192, 18, 1)','rgba(250, 192, 18, 1)']"
                     data-width="none"
                     data-height="none"
                     data-whitespace="nowrap"

                     data-type="text"
                     data-responsive_offset="on"

                     data-frames='[{"delay":700,"speed":1200,"frame":"0","from":"x:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":900,"frame":"999","to":"x:[-100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
                     data-textAlign="['left','left','left','left']"
                     data-paddingtop="[0,0,0,0]"
                     data-paddingright="[0,0,0,0]"
                     data-paddingbottom="[0,0,0,0]"
                     data-paddingleft="[0,0,0,0]"

                     style="z-index: 7; white-space: nowrap; letter-spacing: px;">Subtitle
                </div>

                <!-- LAYER NR. 10 -->
                <a href="about_companyProfile.php" class="tp-caption fh_button rev-btn "
                     id="slide-2-layer-8"
                     data-x="['left','left','left','left']" data-hoffset="['0','40','40','40']"
                     data-y="['top','top','top','top']" data-voffset="['450','450','450','450']"
                     data-width="none"
                     data-height="none"
                     data-whitespace="nowrap"

                     data-type="button"
                     data-responsive_offset="on"
                     data-responsive="off"
                     data-frames='[{"delay":900,"speed":700,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":700,"frame":"999","to":"y:50px;opacity:0;","ease":"nothing"},{"frame":"hover","speed":"300","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(255,255,255);bg:rgb(255,200,17);bc:rgb(255,200,17);bw:2 2 2 2;"}]'
                     data-textAlign="['inherit','inherit','inherit','inherit']"
                     data-paddingtop="[12,12,12,12]"
                     data-paddingright="[35,35,35,35]"
                     data-paddingbottom="[12,12,12,12]"
                     data-paddingleft="[35,35,35,35]"

                     style="z-index: 9; white-space: nowrap; letter-spacing: 1px;background-color:rgba(0, 0, 0, 0);color:#ffffff;border-color:#ffc811;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">
                    About our Company
                </a>
            </li>
            <!-- SLIDE 3 -->
            <li data-index="rs-3" data-transition="fade" data-slotamount="default" data-hideafterloop="0"
                data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300"
                data-thumb="images/homeSlider/6.jpg"
                data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2=""
                data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9=""
                data-param10="" data-description="">
                <!-- MAIN IMAGE -->
                <img src="images/homeSlider/6.png" alt="" title="Unitex"
                     data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="14"
                     class="rev-slidebg" data-no-retina>
                <!-- LAYERS -->

                <!-- LAYER NR. 11 -->
                <div class="tp-caption fp_title_layer   tp-resizeme"
                     id="slide-3-layer-1"
                     data-x="['left','left','left','left']" data-hoffset="['560','40','40','40']"
                     data-y="['top','top','top','top']" data-voffset="['160','160','160','190']"
                     data-fontsize="['70','48','48','30']"
                     data-lineheight="['80','60','60','30']"
                     data-width="none"
                     data-height="none"
                     data-whitespace="nowrap"

                     data-type="text"
                     data-responsive_offset="on"

                     data-frames='[{"delay":500,"speed":1000,"frame":"0","from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":700,"frame":"999","to":"y:[-100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"nothing"}]'
                     data-textAlign="['left','left','left','left']"
                     data-paddingtop="[0,0,0,0]"
                     data-paddingright="[0,0,0,0]"
                     data-paddingbottom="[0,0,0,0]"
                     data-paddingleft="[0,0,0,0]"

                     style="z-index: 5; white-space: nowrap; letter-spacing: 1px;">Welcome To
                </div>

                <!-- LAYER NR. 12 -->
                <div class="tp-caption fp_title_layer   tp-resizeme"
                     id="slide-3-layer-5"
                     data-x="['left','left','left','left']" data-hoffset="['560','40','40','40']"
                     data-y="['top','top','top','top']" data-voffset="['245','220','220','240']"
                     data-fontsize="['70','48','48','30']"
                     data-lineheight="['80','60','60','30']"
                     data-width="none"
                     data-height="none"
                     data-whitespace="nowrap"

                     data-type="text"
                     data-responsive_offset="on"

                     data-frames='[{"delay":500,"speed":1000,"frame":"0","from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":700,"frame":"999","to":"y:[-100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"nothing"}]'
                     data-textAlign="['left','left','left','left']"
                     data-paddingtop="[0,0,0,0]"
                     data-paddingright="[0,0,0,0]"
                     data-paddingbottom="[0,0,0,0]"
                     data-paddingleft="[0,0,0,0]"

                     style="z-index: 6; white-space: nowrap; letter-spacing: 1px;">UNITEX
                </div>

                <!-- LAYER NR. 13 -->
                <div class="tp-caption fp_content_layer   tp-resizeme"
                     id="slide-3-layer-3"
                     data-x="['left','left','left','left']" data-hoffset="['560','40','40','40']"
                     data-y="['top','top','top','top']" data-voffset="['345','300','300','300']"
                     data-fontsize="['24','20','20','18']"
                     data-lineheight="['36','32','32','20']"
                     data-fontweight="['400','700','700','700']"
                     data-color="['rgb(255,200,17)','rgba(250, 192, 18, 1)','rgba(250, 192, 18, 1)','rgba(250, 192, 18, 1)']"
                     data-width="none"
                     data-height="none"
                     data-whitespace="nowrap"

                     data-type="text"
                     data-responsive_offset="on"

                     data-frames='[{"delay":700,"speed":1200,"frame":"0","from":"x:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":900,"frame":"999","to":"x:[-100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
                     data-textAlign="['left','left','left','left']"
                     data-paddingtop="[0,0,0,0]"
                     data-paddingright="[0,0,0,0]"
                     data-paddingbottom="[0,0,0,0]"
                     data-paddingleft="[0,0,0,0]"

                     style="z-index: 7; white-space: nowrap; letter-spacing: px;"> Subtitle
                </div>

                <!-- LAYER NR. 15 -->
                <a href="contact.php" class="tp-caption fh_button rev-btn "
                     id="slide-3-layer-8"
                     data-x="['left','left','left','left']" data-hoffset="['560','40','40','40']"
                     data-y="['top','top','top','top']" data-voffset="['450','450','450','450']"
                     data-width="none"
                     data-height="none"
                     data-whitespace="nowrap"

                     data-type="button"
                     data-responsive_offset="on"
                     data-responsive="off"
                     data-frames='[{"delay":900,"speed":700,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":700,"frame":"999","to":"y:50px;opacity:0;","ease":"nothing"},{"frame":"hover","speed":"300","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(255,255,255);bg:rgb(255,200,17);bc:rgb(255,200,17);bw:2 2 2 2;"}]'
                     data-textAlign="['inherit','inherit','inherit','inherit']"
                     data-paddingtop="[12,12,12,12]"
                     data-paddingright="[35,35,35,35]"
                     data-paddingbottom="[12,12,12,12]"
                     data-paddingleft="[35,35,35,35]"

                     style="z-index: 9; white-space: nowrap; letter-spacing: 1px;background-color:rgba(0, 0, 0, 0);border-color:rgb(255,255,255);outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">
                    Contact Us
                </a>
            </li>
        </ul>
        <script>var htmlDiv = document.getElementById("rs-plugin-settings-inline-css");
            var htmlDivCss = "";
            if (htmlDiv) {
                htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
            } else {
                var htmlDiv = document.createElement("div");
                htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
                document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
            }
        </script>
        <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
    </div>
    <script>var htmlDiv = document.getElementById("rs-plugin-settings-inline-css");
        var htmlDivCss = ".tp-caption.fp_title_layer,.fp_title_layer{color:rgba(255,255,255,1);font-size:70px;line-height:80px;font-weight:700;font-style:normal;font-family:Montserrat;text-decoration:none;background-color:transparent;border-color:transparent;border-style:none;border-width:0 0 0 0px;border-radius:0 0 0 0px}.tp-caption.fp_content_layer,.fp_content_layer{color:#ffc811;font-size:24px;line-height:36px;font-weight:400;font-style:normal;font-family:Roboto;text-decoration:none;background-color:transparent;border-color:transparent;border-style:none;border-width:0 0 0 0px;border-radius:0 0 0 0px}.tp-caption.fh_button,.fh_button{color:rgba(255,255,255,1);font-size:16px;line-height:16px;font-weight:700;font-style:normal;font-family:Open Sans;text-decoration:none;background-color:rgba(0,0,0,0);border-color:#ffffff;border-style:solid;border-width:2 2 2 2px;border-radius:30px 30px 30px 30px}.tp-caption.fh_button:hover,.fh_button:hover{color:#ffffff;text-decoration:none;background-color:#ffc811;border-color:#ffc811;border-style:solid;border-width:2 2 2 2px;border-radius:30px 30px 30px 30px;cursor:pointer}";
        if (htmlDiv) {
            htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
        } else {
            var htmlDiv = document.createElement("div");
            htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
            document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
        }
    </script>
    <script type="text/javascript">
        /******************************************
         -    PREPARE PLACEHOLDER FOR SLIDER    -
         ******************************************/

        var setREVStartSize = function () {
            try {
                var e = new Object, i = jQuery(window).width(), t = 9999, r = 0, n = 0, l = 0, f = 0, s = 0, h = 0;
                e.c = jQuery('#rev_slider_1_1');
                e.responsiveLevels = [1240, 1024, 778, 480];
                e.gridwidth = [1170, 1024, 778, 480];
                e.gridheight = [670, 768, 960, 720];

                e.sliderLayout = "auto";
                if (e.responsiveLevels && (jQuery.each(e.responsiveLevels, function (e, f) {
                        f > i && (t = r = f, l = e), i > f && f > r && (r = f, n = e)
                    }), t > r && (l = n)), f = e.gridheight[l] || e.gridheight[0] || e.gridheight, s = e.gridwidth[l] || e.gridwidth[0] || e.gridwidth, h = i / s, h = h > 1 ? 1 : h, f = Math.round(h * f), "fullscreen" == e.sliderLayout) {
                    var u = (e.c.width(), jQuery(window).height());
                    if (void 0 != e.fullScreenOffsetContainer) {
                        var c = e.fullScreenOffsetContainer.split(",");
                        if (c) jQuery.each(c, function (e, i) {
                            u = jQuery(i).length > 0 ? u - jQuery(i).outerHeight(!0) : u
                        }), e.fullScreenOffset.split("%").length > 1 && void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 ? u -= jQuery(window).height() * parseInt(e.fullScreenOffset, 0) / 100 : void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 && (u -= parseInt(e.fullScreenOffset, 0))
                    }
                    f = u
                } else void 0 != e.minHeight && f < e.minHeight && (f = e.minHeight);
                e.c.closest(".rev_slider_wrapper").css({height: f})

            } catch (d) {
                console.log("Failure at Presize of Slider:" + d)
            }
        };

        setREVStartSize();

        var tpj = jQuery;

        var revapi1;
        tpj(document).ready(function () {
            if (tpj("#rev_slider_1_1").revolution == undefined) {
                revslider_showDoubleJqueryError("#rev_slider_1_1");
            } else {
                revapi1 = tpj("#rev_slider_1_1").show().revolution({
                    sliderType: "standard",
                    jsFileLocation: "//demo2.steelthemes.com/factoryhub/wp-content/plugins/revslider/public/assets/js/",
                    sliderLayout: "auto",
                    dottedOverlay: "none",
                    delay: 9000,
                    navigation: {
                        keyboardNavigation: "on",
                        keyboard_direction: "horizontal",
                        mouseScrollNavigation: "off",
                        mouseScrollReverse: "default",
                        onHoverStop: "off",
                        touch: {
                            touchenabled: "on",
                            touchOnDesktop: "off",
                            swipe_threshold: 75,
                            swipe_min_touches: 1,
                            swipe_direction: "horizontal",
                            drag_block_vertical: false
                        }
                        ,
                        arrows: {
                            style: "factoryplus-arrow",
                            enable: true,
                            hide_onmobile: false,
                            hide_under: 786, //1365 //1024
                            hide_onleave: false,
                            tmp: '<div class="right-arrow">	<i class="fa fa-chevron-right" aria-hidden="true"></i></div><div class="left-arrow">	<i class="fa fa-chevron-left" aria-hidden="true"></i></div>',
                            left: {
                                h_align: "left",
                                v_align: "center",
                                h_offset: 40,
                                v_offset: 0
                            },
                            right: {
                                h_align: "right",
                                v_align: "center",
                                h_offset: 40,
                                v_offset: 0
                            }
                        }
                    },
                    responsiveLevels: [1240, 1024, 778, 480],
                    visibilityLevels: [1240, 1024, 778, 480],
                    gridwidth: [1170, 1024, 778, 480],
                    gridheight: [670, 768, 960, 720],
                    lazyType: "none",
                    parallax: {
                        type: "mouse",
                        origo: "enterpoint",
                        speed: 400,
                        levels: [5, 10, 15, 20, 25, 30, 35, 40, 45, 46, 47, 48, 49, 50, 51, 55],
                        disable_onmobile: "on"
                    },
                    shadow: 0,
                    spinner: "spinner0",
                    stopLoop: "off",
                    stopAfterLoops: -1,
                    stopAtSlide: -1,
                    shuffle: "off",
                    autoHeight: "off",
                    disableProgressBar: "on",
                    hideThumbsOnMobile: "off",
                    hideSliderAtLimit: 0,
                    hideCaptionAtLimit: 0,
                    hideAllCaptionAtLilmit: 0,
                    debugMode: false,
                    fallbacks: {
                        simplifyAll: "off",
                        nextSlideOnWindowFocus: "off",
                        disableFocusListener: false,
                    }
                });
            }
        });
        /*ready*/
    </script>
    <script>
        var htmlDivCss = unescape(".factoryplus-arrow%20%7B%0A%09background-color%3A%20transparent%3B%0A%20%20%20%20border%3A%202px%20solid%20%23fff%3B%0A%20%20%20%20min-width%3A%2055px%3B%0A%20%20%20%20min-height%3A%2055px%3B%0A%20%20%20%20transition%3A%200.5s%3B%0A%7D%0A%0A.factoryplus-arrow%3Ahover%20%7B%0A%20%20%20%20border-color%3A%20%23ffc811%3B%0A%20%20%20%20background-color%3A%20transparent%3B%0A%7D%0A%0A.factoryplus-arrow%3Abefore%20%7B%0A%09display%3A%20none%3B%0A%7D%0A%0A.factoryplus-arrow%3Ahover%20.fa%20%7B%0A%09color%3A%20%23ffc811%3B%0A%7D%0A%0A.factoryplus-arrow%20.fa%20%7B%0A%09color%3A%20%23fff%3B%0A%20%20%20%20font-size%3A%2018px%3B%0A%20%20%20%20position%3A%20absolute%3B%0A%20%20%20%20top%3A%2050%25%3B%0A%20%20%20%20left%3A%2050%25%3B%0A%20%20%20%20transform%3A%20translate%28-50%25%2C-50%25%29%3B%0A%20%20%20%20transition%3A%200.5s%3B%0A%7D%0A%0A.factoryplus-arrow.tp-leftarrow%20.right-arrow%7B%0A%09display%3A%20none%3B%0A%7D%0A%0A.factoryplus-arrow.tp-rightarrow%20.left-arrow%7B%0A%09display%3A%20none%3B%0A%7D%0A");
        var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
        if (htmlDiv) {
            htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
        }
        else {
            var htmlDiv = document.createElement('div');
            htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
            document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
        }
    </script>
</div>