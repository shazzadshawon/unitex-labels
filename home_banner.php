<div class="vc_row wpb_row vc_row-fluid vc_custom_1493345943865 vc_row-has-fill parallax no-parallax-mobile">
    <div class="container">
        <div class="row">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner ">
                    <div class="wpb_wrapper">
                        <div class="vc_row wpb_row vc_inner vc_row-fluid">
                            <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-4 vc_col-md-4">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class=" fh-icon-box-4 dark-version icon-center"
                                             style="background-image: url()"><span class="fh-icon"><i
                                                    class="flaticon-tool"></i></span> <h4>Quality Assurance</h4>
                                            <div class="desc"><p>Quality Assurance related description. Quality Assurance related description. Quality Assurance related description. Quality Assurance related description</p>
                                            </div>
                                            <a class="icon-box-link" href="#" target="_self">Read More<i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></div>
                                    </div>
                                </div>
                            </div>
                            <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-4 vc_col-md-4">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class=" fh-icon-box-4 dark-version icon-center"
                                             style="background-image: url()"><span class="fh-icon"><i
                                                    class="flaticon-business"></i></span> <h4>Sustainability</h4>
                                            <div class="desc"><p>Sustainability related description. Sustainability related description. Sustainability related description. Sustainability related description. Sustainability related description.</p>
                                            </div>
                                            <a class="icon-box-link" href="#" target="_self">Read More<i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></div>
                                    </div>
                                </div>
                            </div>
                            <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-4 vc_col-md-4">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class=" fh-icon-box-4 dark-version icon-center"
                                             style="background-image: url()"><span class="fh-icon"><i
                                                    class="flaticon-nature"></i></span> <h4>Green Technology</h4>
                                            <div class="desc"><p>Green Technology related description. Green Technology related description. Green Technology related description. Green Technology related description.</p>
                                            </div>
                                            <a class="icon-box-link" href="#" target="_self">Read More<i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>