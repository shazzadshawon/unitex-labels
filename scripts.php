<script>if (typeof google !== "object" || typeof google.maps !== "object")
        document.write('<script src="//maps.google.com/maps/api/js?key=AIzaSyBQ4FteMqzRAy5GDrfzMtBVBRV295ZDpKg"><\/script>')</script>
<script type="text/javascript">(function () {
        function addEventListener(element, event, handler) {
            if (element.addEventListener) {
                element.addEventListener(event, handler, false);
            } else if (element.attachEvent) {
                element.attachEvent('on' + event, handler);
            }
        }

        function maybePrefixUrlField() {
            if (this.value.trim() !== '' && this.value.indexOf('http') !== 0) {
                this.value = "http://" + this.value;
            }
        }

        var urlFields = document.querySelectorAll('.mc4wp-form input[type="url"]');
        if (urlFields && urlFields.length > 0) {
            for (var j = 0; j < urlFields.length; j++) {
                addEventListener(urlFields[j], 'blur', maybePrefixUrlField);
            }
        }
        /* test if browser supports date fields */
        var testInput = document.createElement('input');
        testInput.setAttribute('type', 'date');
        if (testInput.type !== 'date') {

            /* add placeholder & pattern to all date fields */
            var dateFields = document.querySelectorAll('.mc4wp-form input[type="date"]');
            for (var i = 0; i < dateFields.length; i++) {
                if (!dateFields[i].placeholder) {
                    dateFields[i].placeholder = 'YYYY-MM-DD';
                }
                if (!dateFields[i].pattern) {
                    dateFields[i].pattern = '[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])';
                }
            }
        }

    })();
</script>

<script type="text/javascript">
    function revslider_showDoubleJqueryError(sliderID) {
        var errorMessage = "Revolution Slider Error: You have some jquery.js library include that comes after the revolution files js include.";
        errorMessage += "<br> This includes make eliminates the revolution slider libraries, and make it not work.";
        errorMessage += "<br><br> To fix it you can:<br>&nbsp;&nbsp;&nbsp; 1. In the Slider Settings -> Troubleshooting set option:  <strong><b>Put JS Includes To Body</b></strong> option to true.";
        errorMessage += "<br>&nbsp;&nbsp;&nbsp; 2. Find the double jquery.js include and remove it.";
        errorMessage = "<span style='font-size:16px;color:#BC0C06;'>" + errorMessage + "</span>";
        jQuery(sliderID).show().html(errorMessage);
    }
</script>
<link rel='stylesheet' id='vc_tta_style-css'
      href='wp-content/plugins/js_composer/assets/css/js_composer_tta.min7661.css?ver=5.4.2' type='text/css'
      media='all'/>
<link rel='stylesheet' id='font-awesome-css'
      href='wp-content/plugins/js_composer/assets/lib/bower/font-awesome/css/font-awesome.min7661.css?ver=5.4.2'
      type='text/css' media='all'/>
<script type='text/javascript'>
    /* <![CDATA[ */
    var wpcf7 = {
        "apiSettings": {
            "root": "http:\/\/demo2.steelthemes.com\/factoryhub\/wp-json\/contact-form-7\/v1",
            "namespace": "contact-form-7\/v1"
        }, "recaptcha": {"messages": {"empty": "Please verify that you are not a robot."}}
    };
    /* ]]> */
</script>
<script type='text/javascript' src='wp-content/plugins/contact-form-7/includes/js/scripts20fd.js?ver=4.9.2'></script>
<script type='text/javascript'
        src='wp-content/plugins/js_composer/assets/lib/bower/flexslider/jquery.flexslider-min7661.js?ver=5.4.2'></script>
<script type='text/javascript'
        src='wp-content/plugins/woocommerce/assets/js/photoswipe/photoswipe.min0235.js?ver=4.1.1'></script>
<script type='text/javascript'
        src='wp-content/plugins/woocommerce/assets/js/photoswipe/photoswipe-ui-default.min0235.js?ver=4.1.1'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var wc_single_product_params = {
        "i18n_required_rating_text": "Please select a rating",
        "review_rating_required": "yes",
        "flexslider": {
            "rtl": false,
            "animation": "slide",
            "smoothHeight": true,
            "directionNav": false,
            "controlNav": "thumbnails",
            "slideshow": false,
            "animationSpeed": 500,
            "animationLoop": false,
            "allowOneSlide": false
        },
        "zoom_enabled": "",
        "photoswipe_enabled": "1",
        "photoswipe_options": {
            "shareEl": false,
            "closeOnScroll": false,
            "history": false,
            "hideAnimationDuration": 0,
            "showAnimationDuration": 0
        },
        "flexslider_enabled": "1"
    };
    /* ]]> */
</script>
<script type='text/javascript'
        src='wp-content/plugins/woocommerce/assets/js/frontend/single-product.minc169.js?ver=3.2.6'></script>
<script type='text/javascript'
        src='wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min44fd.js?ver=2.70'></script>
<script type='text/javascript'
        src='wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min44fd.js?ver=2.70'></script>
<script type='text/javascript'
        src='wp-content/plugins/woocommerce/assets/js/js-cookie/js.cookie.min6b25.js?ver=2.1.4'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var woocommerce_params = {
        "ajax_url": "\/factoryhub\/wp-admin\/admin-ajax.php",
        "wc_ajax_url": "http:\/\/demo2.steelthemes.com\/factoryhub\/?wc-ajax=%%endpoint%%"
    };
    /* ]]> */
</script>
<script type='text/javascript'
        src='wp-content/plugins/woocommerce/assets/js/frontend/woocommerce.minc169.js?ver=3.2.6'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var wc_cart_fragments_params = {
        "ajax_url": "\/factoryhub\/wp-admin\/admin-ajax.php",
        "wc_ajax_url": "http:\/\/demo2.steelthemes.com\/factoryhub\/?wc-ajax=%%endpoint%%",
        "fragment_name": "wc_fragments_2c6ad094ec87a3abe4eec279d523fb3b"
    };
    /* ]]> */
</script>
<script type='text/javascript'
        src='wp-content/plugins/woocommerce/assets/js/frontend/cart-fragments.minc169.js?ver=3.2.6'></script>
<script type='text/javascript' src='wp-content/themes/factoryhub/js/plugins.min79d4.js?ver=20161025'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var factoryhub = {"factoryhub_back": "Back", "direction": ""};
    var factoryhubShortCode = {
        "service": {
            "service-slider-5ad2f2243ec20": {
                "autoplay": 5000,
                "columns": "4",
                "iscarousel": 1
            }
        },
        "post": {"post-slider-5ad2f2250bfc7": {"autoplay": false, "columns": "4"}},
        "testimonial": {"testimonial-slider-5ad2f2251b907": {"autoplay": 5000, "iscarousel": 1, "columns": "3"}}
    };
    /* ]]> */
</script>
<script type='text/javascript' src='wp-content/themes/factoryhub/js/scripts.min79d4.js?ver=20161025'></script>
<script type='text/javascript'
        src='wp-content/plugins/factoryhub-style-switcher/js/switcher6624.js?ver=22112016'></script>
<script type='text/javascript'
        src='wp-content/plugins/variation-swatches-for-woocommerce/assets/js/frontendd0f1.js?ver=20160615'></script>
<script type='text/javascript' src='wp-includes/js/wp-embed.mincbf4.js?ver=4.9.4'></script>
<script type='text/javascript'
        src='https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfontd92a.js?ver=3.0.22'></script>
<script type='text/javascript'
        src='wp-content/plugins/js_composer/assets/js/dist/js_composer_front.min7661.js?ver=5.4.2'></script>
<script type='text/javascript'
        src='wp-content/plugins/js_composer/assets/lib/vc_accordion/vc-accordion.min7661.js?ver=5.4.2'></script>
<script type='text/javascript'
        src='wp-content/plugins/js_composer/assets/lib/vc-tta-autoplay/vc-tta-autoplay.min7661.js?ver=5.4.2'></script>
<script type='text/javascript' src='wp-content/plugins/factoryhub-vc-addon/assets/js/plugins8a54.js?ver=1.0.0'></script>
<script type='text/javascript'
        src='wp-content/plugins/factoryhub-vc-addon/assets/js/frontend8a54.js?ver=1.0.0'></script>

<script type='text/javascript'
        src='wp-content/plugins/woocommerce/assets/js/jquery-ui-touch-punch/jquery-ui-touch-punch.minc169.js?ver=3.2.6'></script>
<script type='text/javascript'
        src='wp-content/plugins/woocommerce/assets/js/accounting/accounting.minaffb.js?ver=0.4.2'></script>
<script type='text/javascript'
        src='wp-content/plugins/woocommerce/assets/js/frontend/price-slider.minc169.js?ver=3.2.6'></script>

<script type='text/javascript' src='wp-includes/js/jquery/ui/core.mine899.js?ver=1.11.4'></script>
<script type='text/javascript' src='wp-includes/js/jquery/ui/widget.mine899.js?ver=1.11.4'></script>
<script type='text/javascript' src='wp-includes/js/jquery/ui/mouse.mine899.js?ver=1.11.4'></script>
<script type='text/javascript' src='wp-includes/js/jquery/ui/slider.mine899.js?ver=1.11.4'></script>

<script type='text/javascript'>
    /* <![CDATA[ */
    var mc4wp_forms_config = [];
    /* ]]> */
</script>
<script type='text/javascript'
        src='wp-content/plugins/mailchimp-for-wp/assets/js/forms-api.min79ab.js?ver=4.1.14'></script>
<!--[if lte IE 9]>
<script type='text/javascript'
        src='wp-content/plugins/mailchimp-for-wp/assets/js/third-party/placeholders.min.js?ver=4.1.14'></script>
<![endif]-->


<!-- start-smoth-scrolling -->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $(".scroll").click(function (event) {
            event.preventDefault();
            $('html,body').animate({scrollTop: $(this.hash).offset().top}, 1000);
        });
    });
</script>
<!-- start-smoth-scrolling -->
<script src="js/bootstrap.min.js"></script><!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->