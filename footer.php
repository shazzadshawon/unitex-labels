<div id="footer-widgets" class="footer-widgets widgets-area">
    <div class="container">
        <div class="row">

            <div class="footer-sidebar footer-1 col-xs-12 col-sm-6 col-md-3">
                <div id="text-11" class="widget widget_text">
                    <div class="textwidget"><a href="index.php" class="footer-logo"><img
                                src="images/logo_4.png" alt="UNITEX"
                                class="logo-light hide-logo" style="height: 100px">
                        </a>
                        <p style="line-height: 2;">
                            Company related text goes here. Company related text goes here. Company related text goes here. Company related text goes here. Company related text goes here.
                        </p></div>
                </div>
            </div>
            <div class="footer-sidebar footer-2 col-xs-12 col-sm-6 col-md-3">
                <div id="nav_menu-2" class="widget widget_nav_menu"><h4 class="widget-title">Factory & Units</h4>
                    <div class="menu-service-menu-container">
                        <ul id="menu-service-menu" class="menu">
                            <li id="menu-item-187"
                                class="menu-item menu-item-type-post_type menu-item-object-service menu-item-187"><a
                                    href="factory.php#1">Capacities &amp; Machinaries</a></li>
                            <li id="menu-item-188"
                                class="menu-item menu-item-type-post_type menu-item-object-service menu-item-188"><a
                                    href="factory.php#4">Environment Management</a></li>
                            <li id="menu-item-189"
                                class="menu-item menu-item-type-post_type menu-item-object-service menu-item-189"><a
                                    href="factory.php#2">Location</a></li>
                            <li id="menu-item-190"
                                class="menu-item menu-item-type-post_type menu-item-object-service menu-item-190"><a
                                    href="factory.php#3">Photos</a></li>

                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer-sidebar footer-3 col-xs-12 col-sm-6 col-md-3">
                <div id="latest-post-widget-2" class="widget latest-post-widget"><h4 class="widget-title">Latest
                        News</h4>
                    <div class="list-post">

                        <div class="latest-post clearfix">
                            <time class="post-date" datetime="2017-04-20T08:57:22+00:00">April 24, 2018</time>
                            <a class="post-title"
                               href="blog_detail.php"
                               title="News Title">News Title</a>
                        </div>

                        <div class="latest-post clearfix">
                            <time class="post-date" datetime="2017-04-20T08:57:22+00:00">April 24, 2018</time>
                            <a class="post-title"
                               href="blog_detail.php"
                               title="News Title">News Title</a>
                        </div>

                    </div>
                </div>
            </div>
            <div class="footer-sidebar footer-4 col-xs-12 col-sm-6 col-md-3">
                <div id="mc4wp_form_widget-2" class="widget widget_mc4wp_form_widget"><h4 class="widget-title">
                        Subscribe Us</h4>
                    <script type="text/javascript">(function () {
                            if (!window.mc4wp) {
                                window.mc4wp = {
                                    listeners: [],
                                    forms: {
                                        on: function (event, callback) {
                                            window.mc4wp.listeners.push({
                                                event: event,
                                                callback: callback
                                            });
                                        }
                                    }
                                }
                            }
                        })();
                    </script>
                    <!-- MailChimp for WordPress v4.1.14 - https://wordpress.org/plugins/mailchimp-for-wp/ -->
                    <form id="mc4wp-form-1" class="mc4wp-form mc4wp-form-185" method="post" data-id="185"
                          data-name="Subscribe Form">
                        <div class="mc4wp-form-fields">
                            <div class="fh-form-field">
                                <p>
                                    Sign up today for tips and latest news and exclusive special offers.
                                </p>
                                <div class="subscribe">
                                    <input type="email" name="EMAIL" placeholder="Enter Your Email" required/>
                                    <input type="submit" value="OK"/>
                                    <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                </div>
                            </div>
                            <label style="display: none !important;">Leave this field empty if you're human: <input
                                    type="text" name="_mc4wp_honeypot" value="" tabindex="-1"
                                    autocomplete="off"/></label><input type="hidden" name="_mc4wp_timestamp"
                                                                       value="1523773989"/><input type="hidden"
                                                                                                  name="_mc4wp_form_id"
                                                                                                  value="185"/><input
                                type="hidden" name="_mc4wp_form_element_id" value="mc4wp-form-1"/></div>
                        <div class="mc4wp-response"></div>
                    </form><!-- / MailChimp for WordPress Plugin --></div>
                <div id="text-8" class="widget widget_text">
                    <div class="textwidget">We don’t do spam and Your mail id is
                        very confidential.
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<footer id="colophon" class="site-footer">
    <div class="container">
        <div class="row">
            <div class="footer-copyright col-md-6 col-sm-12 col-sx-12">
                <div class="site-info">
                    <!--Copyright @ 2018 <a href="#">Unitex</a>, All Right Reserved-->
                    Created with <i class="fa fa-heart" style="color:#fb3533" aria-hidden="true"></i> By <a href="http://shobarjonnoweb.com/" target="_blank">Shobarjonnoweb.com</a>

                </div><!-- .site-info -->
            </div>

            <div class="col-md-6 col-sm-12 col-xs-12 text-right">
                <div class="socials footer-social">
                    <a href="https://facebook.com/" target="_blank"><i class="fa fa-facebook"></i></a><a
                        href="https://twitter.com/" target="_blank"><i class="fa fa-twitter"></i></a><a
                        href="https://www.skype.com/" target="_blank"><i class="fa fa-skype"></i></a><a
                        href="https://www.instagram.com/" target="_blank"><i class="fa fa-instagram"></i></a>
                </div>
            </div>

        </div>
    </div>
</footer><!-- #colophon -->