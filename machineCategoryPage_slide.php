<div id="post-181" class="project-wrapper col-xs-12 col-md-3 col-4 post-181 project type-project status-publish has-post-thumbnail hentry project_category-agriculture project_category-power ">
    <div class="project-inner">
        <a href="machineries_detailsPage.php" class="pro-link"><span class="project-icon">
                                            <i class="fa fa-link" aria-hidden="true"></i></span></a>
        <div class="project-thumbnail">
            <img width="960" height="851"
                 src="images/machinaries/1.jpg"
                 class="attachment-factoryhub-project-full-width-thumb size-factoryhub-project-full-width-thumb wp-post-image"
                 alt=""
                 srcset="images/machinaries/1.jpg 960w, images/machinaries/1.jpg 384w"
                 sizes="(max-width: 960px) 100vw, 960px"/></div>
        <div class="project-summary">
            <h2 class="project-title"><a href="machineries_detailsPage.php">Machine Name</a></h2>
            <div class="project-cat">
                <a href="machineries_detailsPage.php" class="cat">Machine Context One</a>, <a href="machineries_detailsPage.php" class="cat">Machine Context Two</a>
            </div>
        </div>
    </div>
</div>