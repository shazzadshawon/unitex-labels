<div class="wpb_column vc_column_container vc_col-sm-3">
    <div class="vc_column-inner ">
        <div class="wpb_wrapper">
            <div class="fh-team  style-1">
                <div class="team-member">
                    <div class="overlay"
                         style="background-color:rgba(4,25,43,0.77)"></div>
                    <img class=""
                         src="images/profile_placeholder.png"
                         width="270" height="240" alt="team1"
                         title="team1"/>
                </div>
                <div class="info">
                    <h4 class="name">Name</h4>
                    <span class="job">Designation</span>
                    <p>short description</p>
                </div>
                <div class="socials">
                    <ul class="list-social clearfix">
                        <li class="facebook">
                            <a href="#" target="_blank">
                                <i class="fa fa-facebook"></i>
                            </a>
                        </li>
                        <li class="twitter">
                            <a href="#" target="_blank">
                                <i class="fa fa-twitter"></i>
                            </a>
                        </li>
                        <li class="pinterest">
                            <a href="#" target="_blank">
                                <i class="fa fa-pinterest"></i>
                            </a>
                        </li>
                        <li class="linkedin">
                            <a href="#" target="_blank">
                                <i class="fa fa-linkedin"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>