<div class="item-service ">
    <div class="service-content">
        <div class="entry-thumbnail">
            <div class="overlay" style="background-color:rgba(255,200,17,0.8)"></div>
            <a href="#"></a>
            <img width="370" height="230"
                 src="images/370_230.jpg"
                 class="attachment-factoryhub-blog-grid-3-thumb size-factoryhub-blog-grid-3-thumb wp-post-image"
                 alt=""/>
        </div>
        <h2 class="entry-title"><a href="#">Membership Name</a></h2>
        <p>Some Description goes here. Some Description goes here. Some Description goes here </p>
    </div>
</div>