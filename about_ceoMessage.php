<!DOCTYPE html>
<html lang="en-US">

<?php require('header_links.php'); ?>
<style id='factoryhub-inline-css' type='text/css'>
    .site-header .logo img {
        width: -999999999px;
        height: -999999999px;
    }

    .topbar {
        background-color: #f7f7f7;
    }

    .header-title {
        background-image: url(images/banner5_b.jpg);
    }

    .site-footer {
        background-color: #04192b;
    }

    .footer-widgets {
        background-color: #04192b;
    }

    .woocommerce form.checkout h3 input {
        top: 15px;
    }
</style>

<body class="page-template page-template-template-fullwidth page-template-template-fullwidth-php page page-id-196  no-sidebar header-sticky hide-topbar-mobile blog-classic header-v1 footer- wpb-js-composer js-comp-ver-5.4.2 vc_responsive">
<div id="page" class="hfeed site">

    <?php require('header_top_white.php'); ?>
    <?php require('header_mid.php'); ?>


    <div class="page-header title-area style-2">
        <div class="header-title">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="subtitle">About Us</div>
                        <h1 class="page-title">CEO Message <br>
                            Company Mission <br>
                            Company Vision</h1>
                        <div class="page-button-link">
                            <a href="contact.php">Get In Touch with us Today</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="breadcrumb-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <nav class="breadcrumb">
                            <span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
				                <a class="home" href="index.php" itemprop="url"><span itemprop="title">Home</span></a>
			                </span>
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                            <span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                                <span itemprop="title">About Us- CEO Message, Company Mission &amp; Company Vision</span>
                            </span>
                        </nav>
                    </div>

                    <?php require('page_header_share.php'); ?>

                </div>
            </div>
        </div>
    </div>
    <div id="content" class="site-content">

        <div class="container-fluid">
            <div class="row">
                <div id="ceoMessage">
                    <?php require('ceoMessage.php'); ?>
                </div>

                <div class="vc_row wpb_row vc_row-fluid">
                    <div class="container">
                        <div class="row">
                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="vc_separator wpb_content_element vc_separator_align_center vc_sep_width_100 vc_sep_pos_align_center vc_separator_no_text">
                                            <span class="vc_sep_holder vc_sep_holder_l"><span
                                                        style="border-color:#f4f4f4;" class="vc_sep_line"></span></span><span
                                                    class="vc_sep_holder vc_sep_holder_r"><span
                                                        style="border-color:#f4f4f4;" class="vc_sep_line"></span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="vc_row-full-width vc_clearfix"></div>
                <?php require('company_about_banner.php'); ?>

                <div class="vc_row-full-width vc_clearfix"></div>

                <div id="companyMission" style="margin-top: 70px;">
                    <?php require('companyMission.php'); ?>
                </div>

                <?php require('about_numbers.php'); ?>

                <div class="vc_row-full-width vc_clearfix"></div>

                <div id="companyVision">
                    <?php require('companyVision.php'); ?>
                </div>

            </div> <!-- .row -->
        </div> <!-- .container -->
    </div><!-- #content -->


    <?php require('footer.php'); ?>

</div><!-- #page -->

<?php require('common_modal.php'); ?>
<?php require('menu_mobile.php'); ?>

<link rel='stylesheet' id='font-awesome-css'
      href='wp-content/plugins/js_composer/assets/lib/bower/font-awesome/css/font-awesome.min7661.css?ver=5.4.2'
      type='text/css' media='all'/>


<?php require('scripts.php'); ?>

</body>
</html>
