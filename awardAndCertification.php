<div class="vc_row wpb_row vc_row-fluid vc_custom_1493347633289 vc_row-has-fill parallax no-parallax-mobile" style="background-image: url('images/banner_8_w.jpg')!important;">
    <div class="container">
        <div class="row">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner ">
                    <div class="wpb_wrapper">
                        <div class="vc_row wpb_row vc_inner vc_row-fluid vc_custom_1494391099608">
                            <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-3 vc_col-md-6">
                                <div class="vc_column-inner vc_custom_1494391105398">
                                    <div class="wpb_wrapper">
                                        <div class="clearfix  text-left style-2">
                                            <h2 class="awardCertification">Our achieved Awards & Certification</h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-8 vc_col-md-6">
                                <div class="vc_column-inner vc_custom_1494391109734">
                                    <div class="wpb_wrapper">
                                        <div class="wpb_text_column wpb_content_element ">
                                            <div class="wpb_wrapper">
                                                <p style="font-size: 20px; font-weight: 300; color: #252525;">
                                                    We go to great lengths to identify impressive new
                                                    talent self starters with specific skill sets to
                                                    produce truly amazing results.</p>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="wpb_column vc_column_container vc_col-sm-1 vc_col-lg-1 vc_hidden-md vc_hidden-sm">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper"></div>
                                </div>
                            </div>
                        </div>
                        <div class="vc_row wpb_row vc_inner vc_row-fluid">
                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="fh-testimonials  style-1 carousel">
                                            <div class="testi-list " id="testimonial-slider-5ad2f2251b907">

                                                <?php include('awardCertification_slide.php'); ?>
                                                <?php include('awardCertification_slide.php'); ?>
                                                <?php include('awardCertification_slide.php'); ?>
                                                <?php include('awardCertification_slide.php'); ?>
                                                <?php include('awardCertification_slide.php'); ?>
                                                <?php include('awardCertification_slide.php'); ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>