<div class="vc_row wpb_row vc_row-fluid vc_custom_1490605638354">
    <div class="container">
        <div class="row">
            <div class="wpb_column vc_column_container vc_col-sm-6">
                <div class="vc_column-inner ">
                    <div class="wpb_wrapper">
                        <div class="wpb_single_image wpb_content_element vc_align_left">

                            <figure class="wpb_wrapper vc_figure">
                                <div class="vc_single_image-wrapper   vc_box_border_grey"><img
                                        class="vc_single_image-img "
                                        src="images/ceo_placeholder.jpg"
                                        width="570" height="330" alt="about-1" title="about-1"/>
                                </div>
                            </figure>
                        </div>
                    </div>
                </div>
            </div>
            <div class="wpb_column vc_column_container vc_col-sm-6" style="border-left: 4px solid #e0e0e0">
                <div class="vc_column-inner ">
                    <div class="wpb_wrapper">
                        <div class="wpb_text_column wpb_content_element ">
                            <div class="wpb_wrapper">
                                <div style="font-size: 20px; font-weight: 600; color: #222; font-family: 'Open Sans', sans-serif; margin-bottom: 20px;">
                                    This is a General Message or Moto of the Company <br> <span class="main-color">UNITEX Labels</span></div>
                                <p style="margin-bottom: 10px;">Message Text. Message Text. Message Text. Message Text. Message Text. Message Text. Message Text. Message Text. Message Text. Message Text. Message Text. Message Text. Message Text. Message Text. Message Text. Message Text. Message Text. Message Text. Message Text. Message Text. Message Text. Message Text. Message Text. Message Text. </p>
                                <p style="margin-bottom: 10px;">Message Text. Message Text. Message Text. Message Text. Message Text. Message Text. Message Text. Message Text. Message Text. </p>
                                <p style="font-size: 20px; font-weight: 600; color: #222; font-family: 'Open Sans', sans-serif; padding-top: 10px; margin-bottom: 0;">
                                    Name ,</p>
                                <p class="main-color" style="font-size: 20px; font-weight: 300;">CEO
                                    &amp; Founder</p>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>