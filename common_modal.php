<div id="modal" class="modal fade" tabindex="-1" aria-hidden="true">
    <div class="item-detail">
        <div class="modal-dialog woocommerce">
            <div class="modal-content product">
                <div class="modal-header">
                    <button type="button" class="close fh-close-modal" data-dismiss="modal">&#215;<span
                            class="sr-only"></span></button>
                </div>
                <div class="modal-body"></div>
            </div>
        </div>
    </div>
</div>