<!DOCTYPE html>
<html lang="en-US">

<?php require('header_links.php'); ?>
<link href="css/lsb.css" rel="stylesheet" type="text/css">
<link href="css/jquery.fancybox.css" rel="stylesheet" type="text/css" media="all"/>
<link href="css/snow.css" rel="stylesheet" type="text/css" media="all"/><!-- stylesheet -->
<link href="css/myStyle.css" rel="stylesheet" type="text/css" media="all">

<style id='factoryhub-inline-css' type='text/css'>
    .site-header .logo img {
        width: -999999999px;
        height: -999999999px;
    }

    .topbar {
        background-color: #f7f7f7;
    }

    .header-title {
        background-image: url(images/banner11_b.png);
    }

    .site-footer {
        background-color: #04192b;
    }

    .footer-widgets {
        background-color: #04192b;
    }

    .woocommerce form.checkout h3 input {
        top: 15px;
    }
</style>

<body class="page-template page-template-template-fullwidth page-template-template-fullwidth-php page page-id-196  no-sidebar header-sticky hide-topbar-mobile blog-classic header-v1 footer- wpb-js-composer js-comp-ver-5.4.2 vc_responsive">
<div id="page" class="hfeed site">

    <?php require('header_top_white.php'); ?>
    <?php require('header_mid.php'); ?>


    <div class="page-header title-area style-2">
        <div class="header-title">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="subtitle">Factory &amp;</div>
                        <h1 class="page-title">Units</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="breadcrumb-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <nav class="breadcrumb">
                            <span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
				                <a class="home" href="index.php" itemprop="url"><span itemprop="title">Home</span></a>
			                </span>
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                            <span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                                <span itemprop="title">Factory &amp; Units</span>
                            </span>
                        </nav>
                    </div>

                    <?php require('page_header_share.php'); ?>

                </div>
            </div>
        </div>
    </div>
    <div id="content" class="site-content">

        <div id="companyProfile" class="container-fluid">
            <div class="row">


                <div id="1"
                     class="single-project post-176 project type-project status-publish has-post-thumbnail hentry project_category-agriculture project_category-power "
                     style="margin-top:70px; margin-bottom:75px;">
                    <div class="row">

                        <div class="col-md-6 col-sm-12 col-xs-12" style="border-right: 1px solid #ffc811">

                            <div class="row" style="margin-bottom: 0px !important;">
                                <div class="entry-thumbnail col-md-12 col-sm-12 col-xs-12"
                                     style="margin-bottom: 0px !important;">
                                    <div class="item">
                                        <a class="fancybox" rel="portfolio-gallery"><img
                                                    src="images/machine_details/c5-1170x500.jpg"
                                                    alt="c5.jpg"/></a>
                                    </div>
                                    <?php include('factory_capacity_slide.php'); ?>
                                    <?php include('factory_capacity_slide.php'); ?>
                                    <?php include('factory_capacity_slide.php'); ?>
                                    <?php include('factory_capacity_slide.php'); ?>
                                    <?php include('factory_capacity_slide.php'); ?>
                                    <?php include('factory_capacity_slide.php'); ?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="vc_column-inner ">
                                        <div class="vc_single_image-wrapper   vc_box_border_grey"><img
                                                    class="vc_single_image-img img-responsive "
                                                    src="images/about-1.jpg"
                                                    width="100%" height="345" alt="about-1" title="about-1"/>
                                        </div>
                                        <figure class="wpb_wrapper vc_figure">

                                        </figure>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <h2 class="single-project-title">Capacity and Machineries</h2>

                            <div class="entry-content">
                                <p>Machine Details.Machine Details.Machine Details.Machine Details.Machine
                                    Details.Machine Details.Machine Details.Machine Details.Machine Details.Machine
                                    Details.Machine Details.Machine Details.Machine Details.Machine Details.Machine
                                    Details.Machine Details.Machine Details.Machine Details.Machine Details.Machine
                                    Details.</p>
                                <p>Machine Details.Machine Details.Machine Details.Machine Details.Machine
                                    Details.Machine Details.Machine Details.Machine Details.Machine Details.Machine
                                    Details.Machine Details.Machine Details.Machine Details.Machine Details.Machine
                                    Details.Machine Details.Machine Details.Machine Details.Machine Details.Machine
                                    Details.</p>
                                <p>Machine Details.Machine Details.Machine Details.Machine Details.Machine
                                    Details.Machine Details.Machine Details.Machine Details.Machine Details.Machine
                                    Details.Machine Details.Machine Details.Machine Details.Machine Details.Machine
                                    Details.Machine Details.Machine Details.Machine Details.Machine Details.Machine
                                    Details.</p>

                                <div class="project-socials">
                                    <div class="socials">
                                        <a href="https://facebook.com/" target="_blank">
                                            <i class="fa fa-facebook"></i></a><a href="https://twitter.com/"
                                                                                 target="_blank">
                                            <i class="fa fa-twitter"></i></a><a href="https://dribbble.com/"
                                                                                target="_blank">
                                            <i class="fa fa-dribbble"></i>
                                        </a>
                                        <a href="https://www.skype.com/en/" target="_blank">
                                            <i class="fa fa-skype"></i>
                                        </a>
                                        <a href="https://plus.google.com/" target="_blank">
                                            <i class="fa fa-google-plus"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>


                <?php require('company_about_banner.php'); ?>

                <div id="4" class="vc_row-full-width vc_clearfix"></div>

                <div id="boardandmanagement" class="vc_row wpb_row vc_row-fluid vc_custom_1490597375444"
                     style="margin-top: 70px;">
                    <div class="container">
                        <div class="row">
                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="fh-section-title clearfix  text-center style-1"
                                             style="margin-bottom: 50px;">
                                            <h2>Environment Management</h2>
                                        </div>
                                        <!--<div class="vc_row wpb_row vc_inner vc_row-fluid vc_custom_1490588863772">

                                            <?php /*include('prodile_boardManagement_slide.php'); */ ?>
                                            <?php /*include('prodile_boardManagement_slide.php'); */ ?>
                                            <?php /*include('prodile_boardManagement_slide.php'); */ ?>
                                            <?php /*include('prodile_boardManagement_slide.php'); */ ?>
                                            <?php /*include('prodile_boardManagement_slide.php'); */ ?>
                                            <?php /*include('prodile_boardManagement_slide.php'); */ ?>
                                            <?php /*include('prodile_boardManagement_slide.php'); */ ?>

                                        </div>-->

                                        <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6"
                                             style="border-right:1px solid #ffc811;">
                                            <div class="vc_column-inner ">
                                                <div class="wpb_wrapper">
                                                    <div class="vc_tta-container" data-vc-action="collapse">
                                                        <div class="vc_general vc_tta vc_tta-accordion vc_tta-color-grey vc_tta-style-classic vc_tta-shape-square vc_tta-o-shape-group vc_tta-gap-10 vc_tta-controls-align-left fh-accordion fh-accordion">
                                                            <div class="vc_tta-panels-container">
                                                                <div class="vc_tta-panels">

                                                                    <div class="vc_tta-panel vc_active"
                                                                         id="factorytab1"
                                                                         data-vc-content=".vc_tta-panel-body">
                                                                        <div class="vc_tta-panel-heading"><h4
                                                                                    class="vc_tta-panel-title"><a
                                                                                        href="#factorytab1"
                                                                                        data-vc-accordion
                                                                                        data-vc-container=".vc_tta-container"><span
                                                                                            class="vc_tta-title-text">Re-Newable Energy</span><i
                                                                                            class="vc_tta-icon fa fa-angle-down"></i></a>
                                                                            </h4></div>
                                                                        <div class="vc_tta-panel-body">
                                                                            <div class="wpb_text_column wpb_content_element ">
                                                                                <div class="wpb_wrapper">
                                                                                    <p>This Section is Dynamic. Can be
                                                                                        added from the Admin Panel. This
                                                                                        Title and Description is also
                                                                                        Dynamic. This Section is
                                                                                        Dynamic. Can be added from the
                                                                                        Admin Panel. This Title and
                                                                                        Description is also Dynamic.</p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="vc_tta-panel"
                                                                         id="factorytab2"
                                                                         data-vc-content=".vc_tta-panel-body">
                                                                        <div class="vc_tta-panel-heading"><h4
                                                                                    class="vc_tta-panel-title"><a
                                                                                        href="#factorytab2"
                                                                                        data-vc-accordion
                                                                                        data-vc-container=".vc_tta-container"><span
                                                                                            class="vc_tta-title-text">New Topic</span><i
                                                                                            class="vc_tta-icon fa fa-angle-down"></i></a>
                                                                            </h4></div>
                                                                        <div class="vc_tta-panel-body">
                                                                            <div class="wpb_text_column wpb_content_element ">
                                                                                <div class="wpb_wrapper">
                                                                                    <p>This Section is Dynamic. Can be
                                                                                        added from the Admin Panel. This
                                                                                        Title and Description is also
                                                                                        Dynamic. This Section is
                                                                                        Dynamic. Can be added from the
                                                                                        Admin Panel. This Title and
                                                                                        Description is also Dynamic.</p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="vc_tta-panel"
                                                                         id="factorytab3"
                                                                         data-vc-content=".vc_tta-panel-body">
                                                                        <div class="vc_tta-panel-heading"><h4
                                                                                    class="vc_tta-panel-title"><a
                                                                                        href="#factorytab3"
                                                                                        data-vc-accordion
                                                                                        data-vc-container=".vc_tta-container"><span
                                                                                            class="vc_tta-title-text">New Topic</span><i
                                                                                            class="vc_tta-icon fa fa-angle-down"></i></a>
                                                                            </h4></div>
                                                                        <div class="vc_tta-panel-body">
                                                                            <div class="wpb_text_column wpb_content_element ">
                                                                                <div class="wpb_wrapper">
                                                                                    <p>This Section is Dynamic. Can be
                                                                                        added from the Admin Panel. This
                                                                                        Title and Description is also
                                                                                        Dynamic. This Section is
                                                                                        Dynamic. Can be added from the
                                                                                        Admin Panel. This Title and
                                                                                        Description is also Dynamic.</p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <?php require('factory_enivorment_tabs.php'); ?>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <div class="vc_column-inner ">
                                                <div class="vc_single_image-wrapper   vc_box_border_grey"><img
                                                            class="vc_single_image-img img-responsive "
                                                            src="images/about-1.jpg"
                                                            width="100%" height="345" alt="about-1" title="about-1"/>
                                                </div>
                                                <figure class="wpb_wrapper vc_figure">

                                                </figure>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div id="3">
                    <?php require('about_numbers.php'); ?>
                </div>


                <!--PHOTOS-->
                <div id="boardandmanagement" class="vc_row wpb_row vc_row-fluid vc_custom_1490597375444 gallery"
                     style="margin-top: 70px; margin-bottom: 100px;">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="fh-section-title clearfix  text-center style-1">
                                            <h2>Photos</h2>
                                        </div>
                                        <div class="vc_row wpb_row vc_inner vc_row-fluid vc_custom_1490588863772">

                                            <img class="img-responsive loader_image" src="images/loading.gif"
                                                 style="margin: 0 auto">
                                            <section class="gallery-part" id="gallary">
                                                <div class="gallery_loader" style="display: none">
                                                    <div class="tabbing-wrapper" style="display: none">
                                                        <?php include('photo_items.php'); ?>
                                                    </div>
                                                </div>
                                                <div class="gallery-blog">
                                                    <ul class="gallery-img-sec clearfix"></ul>
                                                </div>
                                            </section>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="2">
                    <?php require('about_numbers.php'); ?>
                </div>
                <div>
                    <?php require('contact_locationMap.php'); ?>
                </div>


            </div> <!-- .row -->
        </div> <!-- .container -->
    </div><!-- #content -->


    <?php require('footer.php'); ?>

</div><!-- #page -->

<?php require('common_modal.php'); ?>
<?php require('menu_mobile.php'); ?>

<link rel='stylesheet' id='font-awesome-css'
      href='wp-content/plugins/js_composer/assets/lib/bower/font-awesome/css/font-awesome.min7661.css?ver=5.4.2'
      type='text/css' media='all'/>


<?php require('scripts.php'); ?>
<?php require('contanct_map_scripts.php'); ?>

<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script><!-- Required-js -->
<script src="js/isotope.pkgd.min.js"></script> <!-- Filering -->
<script src="js/packery-mode.pkgd.min.js"></script>
<script src="js/jquery.fancybox.js"></script>
<script src="js/responsiveslides.min.js"></script>

<script>
    // Filltering
    var $container;
    $(document).ready(function () {

        var event_folder = "images/factoryPhotos/";
        $.ajax({
            url: event_folder,
            success: function (event_data) {
                $(event_data).find("a").attr("href", function (i, val) {
                    if (val.match(/\.(jpe?g|png)$/)) {
//                            $("body").append( "<img src='"+ folder + val +"'>" );
                        var src = val;
                        var products = '<li class="main-item grid-sizer all others">' +
                            '<div class="grid">' +
                            '<a href="' + src + '" class="lsb-preview" data-lsb-group="header">' +
                            '<figure class="effect-winston">' +
                            '<img src="' + src + '" class="img-responsive" alt=" " />' +
                            '</figure>' +
                            '</a>' +
                            '</div>' +
                            '</li>';
                        $('.gallery-img-sec').append(products);
                    }
                });
                initIsotope();
            }
        });
    });

    function initIsotope() {
        $container = $('.gallery-img-sec').isotope({
            layoutMode: 'fitRows',
            getSortData: {
                name: '.name',
                symbol: '.symbol',
                number: '.number parseInt',
                category: '[data-category]',
                weight: function( itemElem ) {
                    var weight = $( itemElem ).find('.weight').text();
                    return parseFloat( weight.replace( /[\(\)]/g, '') );
                }
            }
        });
    }

    $(window).load(function () {
        if ($('.gallery-part .gallery-img-sec').length) {
            $('#allButton').css("background", "#ffc811");

            if ($container != undefined) {
                $container.isotope('layout');
            }

            $('.gallery-part .tabbing-wrapper button').on('click', function () {

                var filterValue = "." + $(this).attr('data-filter');
                $container.isotope({
                    filter: filterValue
                });
                var fancybox = $(this).attr('data-filter');
                $(filterValue).find('a').attr({
                    'data-fancybox-group': fancybox
                });

            });
        }
    });

    if ($('.fancybox-button').length) {
        $(".fancybox-button").fancybox({
            prevEffect: 'none',
            nextEffect: 'none',
            closeBtn: true,
            helpers: {
                title: {
                    type: 'inside'
                },
                buttons: {}
            }
        });
    }

    $('.control').on('click', function () {
        $(this).remove();
        var video = '<iframe src="' + $('.video img').attr('data-video') + '"></iframe>'
        $('.video img').after(video);
        return false;
    });
</script>
<!-- js -->

<!-- //main slider-banner -->

<script src="js/lsb.min.js"></script>
<script>
    $(window).load(function () {
        $('.gallery_loader').show();
        // language=JQuery-CSS
        $('.loader_image').hide();
        $.fn.lightspeedBox();
    });
</script>
<!-- //js -->

</body>
</html>
