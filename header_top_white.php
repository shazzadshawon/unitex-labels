<div id="topbar" class="topbar">
    <div class="container">
        <div class="row">

            <div class="topbar-left topbar-widgets text-left col-md-7 col-sm-12 col-xs-12">
                <div id="text-4" class="widget widget_text">
                    <div class="textwidget"><i class="flaticon-pin" aria-hidden="true"></i>Mohakhali DOHS,
                        Dhaka-1206
                    </div>
                </div>
                <div id="text-5" class="widget widget_text">
                    <div class="textwidget"><i class="flaticon-technology" aria-hidden="true"></i> Phone +44 567
                        890123
                    </div>
                </div>
                <div id="text-6" class="widget widget_text">
                    <div class="textwidget"><i class="flaticon-web" aria-hidden="true"></i> Mail@unitex.com</div>
                </div>
            </div>


            <div class="topbar-right topbar-widgets text-right col-md-5 col-sm-12 col-xs-12">
                <div id="text-7" class="widget widget_text">
                    <div class="textwidget">
                        <ul class="topbar-socials">
                            <li><a href="https://www.facebook.com/"><i class="fa fa-facebook"
                                                                       aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-skype" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div id="search-3" class="widget widget_search">
                    <form role="search" method="get" class="search-form"
                          action="index.php">
                        <label>
                            <span class="screen-reader-text">Search for:</span>
                            <input type="search" class="search-field" placeholder="Search &hellip;" value=""
                                   name="s"/>
                        </label>
                        <input type="submit" class="search-submit" value="Search"/>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
<div id="fh-header-minimized" class="fh-header-minimized fh-header-v1"></div>