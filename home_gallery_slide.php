<div class="item-project">
    <div class="project-inner">
        <div class="overlay"
             style="background-color:rgba(2,17,30,0.8)"></div>
        <a href="machineries_detailsPage.php"
           class="pro-link"><span
                class="project-icon"><i
                    class="fa fa-link"
                    aria-hidden="true"></i></span></a>
        <div class="project-thumbnail">
            <img width="384" height="340"
                 src="images/home_gallery/3.jpg"
                 class="attachment-factoryhub-project-carousel-thumb size-factoryhub-project-carousel-thumb wp-post-image"
                 alt=""
                 srcset="images/home_gallery/3.jpg 384w, images/home_gallery/3.jpg 960w"
                 sizes="(max-width: 384px) 100vw, 384px"/>
        </div>
        <div class="project-summary">
            <h2 class="project-title"><a
                    href="machineries_categoryPage.php">Machinery</a>
            </h2>
            <div class="project-cat"><a
                    href="machineries_detailsPage.php"
                    class="cat">Machinery Name </a>, <a
                    href="machineries_detailsPage.php"
                    class="cat">Machinery Name</a></div>
        </div>
    </div>
</div>